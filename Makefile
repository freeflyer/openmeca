
# The install path used when run a 'make install'
ifndef DESTDIR
	DESTDIR=/usr/local
endif

# the bin dir 
BINDIR=bin

# The qmake utility location (for unix-like systems)
QMAKE=qmake



####################################
# Cross compile from unix to win32 #
####################################
# The mxe platform is used. So, you need to specifiy your mxe path. 
# For example, if you type 'mxe=/home/omc/mxe make', 
# it runs a cross-compilation

# if mxe is used, the mxe path must be exported
ifdef mxe
	export PATH := $(mxe)/usr/bin:$(PATH)
	mxe_bin = $(mxe)/usr/i686-w64-mingw32.static/qt5/bin
endif

####################################
# Compile on OS X                  #
####################################
OSName := $(shell uname)

####################################
# Makefile targets                 #
####################################
.PHONY: all message chronoengine qglviewer openmeca

all: message
	$(MAKE) chronoengine
	$(MAKE) qglviewer
	$(MAKE) openmeca


message:
ifdef mxe
	@echo 'cross compiling for win32...' ;\
	sleep 1
endif
	@echo 'compiling...'

clean:
	-rm -rf SRC/ChronoEngine/BUILD
	-rm -rf SRC/ChronoEngine/.qmake.stash
	-rm -rf SRC/ChronoEngine/Makefile
	-rm -rf SRC/ChronoEngine/Makefile.Debug
	-rm -rf SRC/ChronoEngine/Makefile.Release
	-rm -rf SRC/ChronoEngine/object_script.*
	-rm -rf SRC/QGLViewer/BUILD
	-rm -rf SRC/QGLViewer/.qmake.stash
	-rm -rf SRC/QGLViewer/Makefile
	-rm -rf SRC/QGLViewer/Makefile.Debug
	-rm -rf SRC/QGLViewer/Makefile.Release
	-rm -rf SRC/QGLViewer/object_script.*
	-rm -rf SRC/QGLViewer/QGLViewer.prl
	-rm -rf SRC/QGLViewer/QGLViewerd.prl
	-rm -rf SRC/OpenMeca/BUILD
	-rm -rf SRC/OpenMeca/.qmake.stash
	-rm -rf SRC/OpenMeca/Makefile
	-rm -rf SRC/OpenMeca/Makefile.Debug
	-rm -rf SRC/OpenMeca/Makefile.Release
	-rm -rf SRC/OpenMeca/object_script.*
	-rm -rf SRC/OpenMeca/Rsc/Help/Help.qch
	-rm -rf SRC/OpenMeca/Rsc/Help/Help.qhc
	-rm -rf SRC/OpenMeca/Rsc/Lang/openmeca_fr.qm


install: all
	@echo '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'; \
	echo '@@            Installing openmeca              @@'; \
	echo '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'; \
	mkdir -p $(DESTDIR)/$(BINDIR)
	cp -ar ./SRC/OpenMeca/BUILD/openmeca $(DESTDIR)/$(BINDIR)
	@echo 'The openmeca executable was installed in the $(DESTDIR)/$(BINDIR) directory'


uninstall: 
	@echo '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'; \
	echo '@@           Uninstalling openmeca             @@'; \
	echo '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'; \
	rm  $(DESTDIR)/$(BINDIR)/openmeca
	@echo 'The openmeca executable was removed from $(DESTDIR)/$(BINDIR)'


purge: uninstall
	rm -rf $(HOME)/openmeca
	@echo 'The openmeca config directory $(HOME)/openmeca was removed'


chronoengine:
	@echo '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'; \
	echo '@@ making the chronoengine third party library @@'; \
	echo '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'; \
	sleep 1
ifdef mxe
	@echo 'cross compiling chronoengine for win32 !'
	@cd SRC/ChronoEngine ;\
	$(mxe_bin)/qmake ;\
	$(MAKE) install
else
	@cd SRC/ChronoEngine ; \
	$(QMAKE)  ; \
	$(MAKE) install
endif


qglviewer:
	@echo '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'; \
	echo '@@  making the qglviewer third party library   @@'; \
	echo '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'; \
	sleep 1
ifdef mxe
	@echo 'cross compiling qglviewer for win32 !'
	@cd SRC/QGLViewer ;\
	$(mxe_bin)/qmake PREFIX=../BUILD;\
	$(MAKE) install;
else
	@cd SRC/QGLViewer ; \
	$(QMAKE) ; \
	$(MAKE) install
endif


openmeca:
	@echo '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'; \
	echo '@@              making openmeca                @@'; \
	echo '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'; \
	sleep 1
ifdef mxe
	@echo 'cross compiling openmeca for win32 !'
	@cd SRC/OpenMeca  ; \
	lrelease ./OpenMeca.pro  ; \
	qhelpgenerator ./Rsc/Help/Help.qhp -o ./Rsc/Help/Help.qch  ; \
	qcollectiongenerator ./Rsc/Help/Help.qhcp -o ./Rsc/Help/Help.qhc ;\
	$(mxe_bin)/qmake ;\
	$(MAKE)
else
	@cd SRC/OpenMeca  ; \
	lrelease ./OpenMeca.pro  ; \
	qhelpgenerator ./Rsc/Help/Help.qhp -o ./Rsc/Help/Help.qch  ; \
	qcollectiongenerator ./Rsc/Help/Help.qhcp -o ./Rsc/Help/Help.qhc ;\
	$(QMAKE)  ; \
	$(MAKE)
ifeq ($(OSName),Darwin)
	macdeployqt SRC/OpenMeca/BUILD/openmeca.app
endif
endif
	@echo 'The executable was built in the OpenMeca/BUILD/openmeca directory';

