// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef OpenMeca_Gui_Dialog_DialogSystemSettingT_hpp
#define OpenMeca_Gui_Dialog_DialogSystemSettingT_hpp

#include <iostream>
#include <QAction>

#include "OpenMeca/Gui/Dialog/DialogSystemSetting.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Core/AutoRegister.hpp"


namespace OpenMeca
{
  namespace Gui
  {
    template <class T>
    class DialogSystemSettingT : public DialogSystemSetting
   {

   public:
     void Edit(QAction& action, T& item);
      
   protected:
     DialogSystemSettingT();
     virtual ~DialogSystemSettingT();
     
     T& GetSetting();
      
    };

    template <class T>
    inline
    DialogSystemSettingT<T>::DialogSystemSettingT()
      :DialogSystemSetting()
    {
    }
    
    template <class T>
    inline
    DialogSystemSettingT<T>::~DialogSystemSettingT()
    {
    }

    template <class T>
    inline T&
    DialogSystemSettingT<T>::GetSetting()
    {
      return Core::SystemSettingT<T>::Get();
    }
    
    template <class T>
    inline void
    DialogSystemSettingT<T>::Edit(QAction& action, T&)
    {
      Dialog::Show_CallBack();
      DialogSystemSetting::SetAction(action);
      typename T::Dialog* me = static_cast< typename T::Dialog*>(this);
      QString title = QString("Edit ") + QString(T::GetStrType().c_str());
      MainWindow::Get().GetDialogContainer().insertItem(1, me, title);
      MainWindow::Get().GetDialogContainer().setCurrentIndex(1);
      Init();
      me->show();
    }

  }
}

#endif
