// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Gui/Dialog/DialogPartUserPoint.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Item/PartUserPoint.hpp"
#include "OpenMeca/Item/Body.hpp"
#include "OpenMeca/Util/Dimension.hpp"

namespace OpenMeca
{
  namespace Gui
  {
  
    DialogPartUserPoint::DialogPartUserPoint()
      :DialogUserItemT<Item::PartUserPoint>(),
       id_(this),
       parent_(this),
       point_(this),
       visible_(this)
    {
      id_.SetLabel(QObject::tr("Name"));
      parent_.Prop::SetLabel(QObject::tr("Parent Item"));
      point_.SetLabel(QObject::tr("Center"));
      point_.SetDimension(Util::Dimension::Get("Length"));
      visible_.SetLabel(QObject::tr("Visible"));

      // Init table
      GetPropTree().Add(id_);
      GetPropTree().Add(parent_);
      GetPropTree().Add(point_);
      GetPropTree().Add(visible_);
    }
  
    DialogPartUserPoint::~DialogPartUserPoint()
    {
    }

    void
    DialogPartUserPoint::Init()
    {
      point_.SetValue(GetCurrentItem().GetCenterExpr());
      id_.SetValue(GetCurrentItem().GetName());
      visible_.SetValue(GetCurrentItem().GetVisible());
      // populate parent list
      Core::SetOfBase<OpenMeca::Item::Body>& body = 
	Core::System::Get().GetSetOf<OpenMeca::Item::Body>();
      
      Core::SetOfBase<Core::UserItem> set;
      for (unsigned int i = 0; i < body.GetTotItemNumber(); ++i)
	{
	  Core::UserItem& item = body(i);
	  set.AddItem(item);
	}
      
      
      OMC_ASSERT_MSG(set.GetTotItemNumber() > 0, "The number of item is null");
      parent_.SetList(set);
      parent_.SetValue(GetCurrentItem().GetParentItemPtr());
    }
  }
}
