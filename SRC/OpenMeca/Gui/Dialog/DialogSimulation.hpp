// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Gui_DialogSimulation_hpp 
#define OpenMeca_Gui_DialogSimulation_hpp 

#include "ui_DialogSimulation.h"
#include "OpenMeca/Core/Singleton.hpp"
#include "OpenMeca/Gui/Dialog/DialogSystemSettingT.hpp"
#include "OpenMeca/Gui/Prop/PropEnumT.hpp"
#include "OpenMeca/Gui/Prop/PropDouble.hpp"
#include "OpenMeca/Gui/Prop/PropBool.hpp"
#include "OpenMeca/Setting/Simulation.hpp"

#include "ChronoEngine/physics/ChSystem.h"

namespace OpenMeca
{

  namespace Gui
  {

    class DialogSimulation :  public DialogSystemSettingT<Setting::Simulation>, 
			      private Ui::DialogSimulation,
			      public Core::Singleton<DialogSimulation>
    {

      Q_OBJECT
    public:
      DialogSimulation();
      virtual ~DialogSimulation();
      virtual bool Check();
      PropTree& GetPropTree(){return *tree_;}
      bool IsPossibleToClose();
		  
    private slots:
      void Stop();
      void Start();
      void Pause();
      void Close();

    private:
      void Init();
      void Ok();
      
    private :
      PropDouble step_;
      PropDouble time_;
      PropEnumT<chrono::ChSystem::eCh_lcpSolver> solver_;
      PropEnumT<chrono::ChSystem::eCh_integrationType> integrationType_;
      PropDouble animationPeriod_;
      PropBool staticEquilibrium_;
    };
  }
}

#endif
    
