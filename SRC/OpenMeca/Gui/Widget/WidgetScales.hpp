// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Widget_WidgetScales_hpp
#define OpenMeca_Widget_WidgetScales_hpp


#include <QWidget>
#include <QBoxLayout>
#include <QLabel>

#include "ui_WidgetScales.h"


namespace OpenMeca
{
  namespace Gui
  {
    class WidgetScales: public QWidget, 
			private Ui::WidgetScales
    {
      Q_OBJECT
      
      public:
      WidgetScales(QWidget* parent);
      virtual ~WidgetScales();
      
      void Update();
      void IsEdited();	 
      double& GetScaleValue();
		     
		     
    private slots:
      void ValueChanged(double currentValue);
      void ScaleChanged(const QString&);

    private:
      void UpdateStep();
      
    private:
      QAction* editScale_;
      std::map<int, std::string> index_;
      
    };
    
  }
}
#endif

