// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Gui/AbstractTreeItem.hpp"
#include "OpenMeca/Gui/TreeView.hpp"
#include "OpenMeca/Core/UserItem.hpp"

namespace OpenMeca
{
  
  namespace Gui
  {

    TreeView* AbstractTreeItem::treeView_ = 0;

    void AbstractTreeItem::SetTreeView(TreeView& treeView)
    {
      OMC_ASSERT_MSG(treeView_==0, "The treeview is already affected");
      treeView_ = &treeView;
    }

    TreeView& AbstractTreeItem::GetTreeView()
    {
      OMC_ASSERT_MSG(treeView_!=0, "The treeview is null");
      return *treeView_;
    }

     AbstractTreeItem::AbstractTreeItem(QTreeWidgetItem* parent)
       :QTreeWidgetItem(parent,0),
	relatedItemIsDeleted_(false)
    {
      GetTreeView().AddAbstractTreeItem(*this);
    }

    AbstractTreeItem::~AbstractTreeItem()
    {
      GetTreeView().EraseAbstractTreeItem(*this);
    }
    
    void 
    AbstractTreeItem::Update()
    {
      QTreeWidgetItem::setIcon(0,GetItem().GetIcon());
      QTreeWidgetItem::setText(0,GetItem().GetName().c_str());
    }

    void 
    AbstractTreeItem::RelatedItemIsDeleted()
    {
      relatedItemIsDeleted_ = true;
    }
    
    bool
    AbstractTreeItem::IsRelatedItemIsDeleted() const
    {
      return relatedItemIsDeleted_;
    }

  }
}
