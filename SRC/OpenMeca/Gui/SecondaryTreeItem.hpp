// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Gui_SecondaryTreeItem_hpp
#define OpenMeca_Gui_SecondaryTreeItem_hpp

#include "OpenMeca/Gui/AbstractTreeItem.hpp"
#include <functional>

namespace OpenMeca
{
  namespace Core
  {
    class UserItem;
  }
  

  namespace Gui
  {
    
    class SecondaryTreeItem : public AbstractTreeItem
    {
    
    public:
      SecondaryTreeItem(QTreeWidgetItem& parent, std::function<Core::Item& ()>);
      SecondaryTreeItem(QTreeWidgetItem& parent, Core::Item& item);
      ~SecondaryTreeItem();

      Core::Item& GetItem();
    
    private:
      SecondaryTreeItem();                        //Not allowed
      SecondaryTreeItem(const SecondaryTreeItem&);             //Not Allowed
      SecondaryTreeItem& operator=(const SecondaryTreeItem&);  //Not Allowed

    private:
      std::function<Core::Item& ()>* itemFn_;
      Core::Item* itemPtr_;

    };
  }
}
#endif
