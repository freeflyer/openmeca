// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <QMouseEvent>
#include <QTemporaryDir>

#include "OpenMeca/Gui/ExampleAction.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Core/System.hpp"

namespace OpenMeca
{
  
  namespace Gui
  {
    ExampleAction::ExampleAction(QString fileName, QWidget * parent)
      :QAction(QObject::tr(fileName.remove(".omc").toStdString().c_str()), parent),
       fileName_(fileName)
    {
      QObject::connect(this, SIGNAL(triggered()), this, SLOT(OpenExample()));
    }
    
    ExampleAction::~ExampleAction()
    {
    }

    void
    ExampleAction::OpenExample()
    {
      if (!MainWindow::Get().CloseDialog(true))
	return;
      
      QFile file(":/Rsc/Example/"+fileName_+".omc");
      QTemporaryDir dir;
      OMC_ASSERT_MSG(file.copy(dir.path() + fileName_+".omc"), "can't copy file in temporary dir !");
      
      const std::string file_cp = QString(dir.path()+fileName_+".omc").toStdString();
      Core::System::Get().LoadFile(file_cp);
    }
  
    
  }
}
