// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Gui_MainWindow_hpp
#define OpenMeca_Gui_MainWindow_hpp

#include <QMainWindow>
#include <qwt_plot.h>

#include "ui_MainWindow.h"
#include "OpenMeca/Core/GlobalSetting.hpp"
#include "OpenMeca/Gui/Widget/WidgetScales.hpp"
#include "OpenMeca/Gui/MainPlotWindow.hpp"
#include "OpenMeca/Gui/GeneralSetting.hpp"
#include "OpenMeca/Util/Version.hpp"

namespace OpenMeca
{
  namespace Gui
  {
    class Dialog;

    class MainWindow : public QMainWindow, 
		       private Ui::MainWindow
    {
      Q_OBJECT
      
      public:
      static MainWindow& Get();
      static bool Exist();

      MainWindow(std::string openFileName);
      ~MainWindow();
      
      TreeView& GetTreeView();
      const TreeView& GetTreeView() const;

      Viewer& GetViewer();
      const Viewer& GetViewer() const;
      
      bool CloseDialog(bool allowCancellation);
      void AddGlobalSetting(Core::GlobalSetting&);
      void RemoveGlobalSetting(Core::GlobalSetting&);
      QToolBox& GetDialogContainer();
      void DialogContainerIsBusy(Dialog&);
      void DialogContainerIsFree(Dialog&);					 
      void AddToHistoric(QAction&);
      int GetIconSize();
      const QIcon& GetIcon() const;
      void BuildHelpWindow();
      void BuildExampleMenu();

    public slots:
      void Undo();
      void Redo();
      void New();
      void SaveAs();
      void Save();
      void Load();
      void ShowData();
      void SaveData();
      void ShowAbout();
      void ShowHelp();
      void ShowSetting();
      void LoadExample();


    private:
      void UpdateHistoric();
      void closeEvent(QCloseEvent* event);
      void showEvent(QShowEvent *ev);


    private:
      static MainWindow* me_;
      bool init_;
      std::string openFileNameQueryAtStart_;
      const int iconSize_;
      QIcon icon_;
      Core::SetOf<Core::GlobalSetting> globalSet_;
      Dialog* currentDialog_;
      std::vector<QAction*> userActions_;
      std::string currentFileName_;
      WidgetScales* widgetScales_;
      MainPlotWindow* plot_;
      QDockWidget* helpWindow_;
      GeneralSetting* setting_;
      std::string exampleToLoad_;
      // QBoxLayout *dialogContainerLayout_;


      
    };

  }
}
#endif
