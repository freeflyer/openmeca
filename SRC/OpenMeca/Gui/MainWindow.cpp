// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <QFileDialog>
#include <QMessageBox>
#include <QCloseEvent>
#include <QBuffer>
#include <QHelpEngine>
#include <QHelpContentWidget>
#include <QHelpIndexWidget>
#include <QDockWidget>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Gui/HelpBrowser.hpp"
#include "OpenMeca/Gui/Dialog/Dialog.hpp"
#include "OpenMeca/Gui/ExampleAction.hpp"
#include "OpenMeca/Core/CommonProperty.hpp"
#include "OpenMeca/Core/UserRootItemCommonProperty.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Core/ConfigFile.hpp"
#include "OpenMeca/Core/Software.hpp"
#include "OpenMeca/Item/Body.hpp"
#include "OpenMeca/Item/Variable.hpp"

#include "OpenMeca/Item/Link.hpp"
#include "OpenMeca/Item/LinkT.hpp"
#include "OpenMeca/Item/PartLinkT.hpp"
#include "OpenMeca/Item/Link/All.hpp"

#include "OpenMeca/Item/PartPoint.hpp"
#include "OpenMeca/Item/PartUserPoint.hpp"
#include "OpenMeca/Item/PartUserPipe.hpp"
#include "OpenMeca/Item/PartUserJunction.hpp"
#include "OpenMeca/Item/PartUserShapeT.hpp"
#include "OpenMeca/Item/Shape/Sphere.hpp"
#include "OpenMeca/Item/Shape/Box.hpp"
#include "OpenMeca/Item/Shape/Cylinder.hpp"
#include "OpenMeca/Item/Shape/Ground.hpp"

#include "OpenMeca/Item/How/GetLinPos.hpp"
#include "OpenMeca/Item/How/GetLinVel.hpp"
#include "OpenMeca/Item/How/GetLinAcc.hpp"
#include "OpenMeca/Item/How/GetAngPos.hpp"
#include "OpenMeca/Item/How/GetAngVel.hpp"
#include "OpenMeca/Item/How/GetAngAcc.hpp"
#include "OpenMeca/Item/How/GetReacForce.hpp"
#include "OpenMeca/Item/How/GetReacTorque.hpp"
#include "OpenMeca/Item/How/SetForce.hpp"
#include "OpenMeca/Item/How/SetTorque.hpp"


#include "OpenMeca/Setting/MenuManager.hpp"
#include "OpenMeca/Setting/UnitManager.hpp"
#include "OpenMeca/Core/GlobalSettingCommonProperty.hpp"

#include "OpenMeca/Setting/Simulation.hpp"
#include "OpenMeca/Setting/Scales.hpp"
#include "OpenMeca/Setting/Gravity.hpp"
#include "OpenMeca/Setting/Scene.hpp"

namespace OpenMeca
{
  namespace Gui
  {


    
    MainWindow* MainWindow::me_ = 0;

    MainWindow& 
    MainWindow::Get()
    {
      OMC_ASSERT_MSG(me_!=0, "The singleton pointer is null");
      return *me_;
    }

    bool
    MainWindow::Exist()
    {
      return (me_ != 0);
    }


    MainWindow::MainWindow(std::string openFileName)
      :QMainWindow(0),
       init_(true),
       openFileNameQueryAtStart_(openFileName),
       iconSize_(24),
       icon_(),
       currentDialog_(0),
       userActions_(),
       currentFileName_(),
       widgetScales_(0),
       plot_(0),
       helpWindow_(0),
       setting_(0),
       exampleToLoad_("")
    {
      OMC_ASSERT_MSG(iconSize_ > 0, "The icon size must be higher than zero");
      OMC_ASSERT_MSG(me_==0, "The singleton pointer must be null");
      me_ = this;
      setupUi(this);
      icon_ = Util::Icon::DrawIconFromSvgFile(":/Rsc/Img/Mini-Logo.svg");
      setWindowIcon(icon_);
      BuildHelpWindow();
      treeView_->setIconSize(QSize(iconSize_,iconSize_));
      treeView_->setAutoScrollMargin(iconSize_);
      treeView_->headerItem()->setText(0, tr("Mechanical System"));

      // The system setting must be placed at first
      Setting::Scales::Init();
      Setting::Gravity::Init();
      Setting::Simulation::Init();
      // Note that the Scene::Init() has moved in the showEvent because of a qt bug


      // Init widget
      widgetScales_ = new WidgetScales(this),
      plot_         = new MainPlotWindow(this);
      toolBar_->addWidget(widgetScales_);
      setting_ = new GeneralSetting(this);

      // Instanciate user root item
      Core::Singleton<Core::UserRootItemCommonProperty<Item::Variable> >::Instanciate();      
      Core::Singleton<Core::UserRootItemCommonProperty<Item::Body> >::Instanciate();
      Core::Singleton<Core::UserRootItemCommonProperty<Item::Link> >::Instanciate();

      // Init variable
      Item::Variable::Init();

     

      // And after the item
      Item::PartUserJunction::Init();
      Item::PartUserPipe::Init();
      Item::PartPoint::Init();
      Item::PartUserPoint::Init();
      Item::PartUserShapeT<Item::Shape::Sphere>::Init();
      Item::PartUserShapeT<Item::Shape::Box>::Init();
      Item::PartUserShapeT<Item::Shape::Cylinder>::Init();
      Item::PartUserShapeT<Item::Shape::Ground>::Init();
      
      Item::Body::Init();
      Item::Link::Init();
      

      Item::LinkT<Item::Revolute>::Init();
      Item::PartLinkT<Item::Revolute,1>::Init();
      Item::PartLinkT<Item::Revolute,2>::Init();

      Item::LinkT<Item::Motor>::Init();
      Item::PartLinkT<Item::Motor,1>::Init();
      Item::PartLinkT<Item::Motor,2>::Init();

      Item::LinkT<Item::LinearMotor>::Init();
      Item::PartLinkT<Item::LinearMotor,1>::Init();
      Item::PartLinkT<Item::LinearMotor,2>::Init();

      Item::LinkT<Item::Slider>::Init();
      Item::PartLinkT<Item::Slider,1>::Init();
      Item::PartLinkT<Item::Slider,2>::Init();

      Item::LinkT<Item::Cylindrical>::Init();
      Item::PartLinkT<Item::Cylindrical,1>::Init();
      Item::PartLinkT<Item::Cylindrical,2>::Init();

      Item::LinkT<Item::Planar>::Init();
      Item::PartLinkT<Item::Planar,1>::Init();
      Item::PartLinkT<Item::Planar,2>::Init();

      Item::LinkT<Item::PointLine>::Init();
      Item::PartLinkT<Item::PointLine,1>::Init();
      Item::PartLinkT<Item::PointLine,2>::Init();

      Item::LinkT<Item::PointPlane>::Init();
      Item::PartLinkT<Item::PointPlane,1>::Init();
      Item::PartLinkT<Item::PointPlane,2>::Init();

      Item::LinkT<Item::Spherical>::Init();
      Item::PartLinkT<Item::Spherical,1>::Init();
      Item::PartLinkT<Item::Spherical,2>::Init();

      Item::LinkT<Item::Screw>::Init();
      Item::PartLinkT<Item::Screw,1>::Init();
      Item::PartLinkT<Item::Screw,2>::Init();

      Item::LinkT<Item::Gear>::Init();
      Item::PartLinkT<Item::Gear,1>::Init();
      Item::PartLinkT<Item::Gear,2>::Init();

      Item::LinkT<Item::Pulley>::Init();
      Item::PartLinkT<Item::Pulley,1>::Init();
      Item::PartLinkT<Item::Pulley,2>::Init();

      Item::LinkT<Item::RackPinion>::Init();
      Item::PartLinkT<Item::RackPinion,1>::Init();
      Item::PartLinkT<Item::RackPinion,2>::Init();

      Item::LinkT<Item::Spring>::Init();
      Item::PartLinkT<Item::Spring,1>::Init();
      Item::PartLinkT<Item::Spring,2>::Init();


      Item::How::GetLinPos::MySensor::Init();
      Item::How::GetLinVel::MySensor::Init();
      Item::How::GetLinAcc::MySensor::Init();
      Item::How::GetAngPos::MySensor::Init();     
      Item::How::GetAngVel::MySensor::Init();     
      Item::How::GetAngAcc::MySensor::Init();     
      Item::How::GetReacForce::MySensor::Init();     
      Item::How::GetReacTorque::MySensor::Init();     
      Item::How::SetForce::MyLoad::Init();
      Item::How::SetTorque::MyLoad::Init();

      

      //Init the globalsetting (Always at the end)
      Setting::MenuManager::Init();
      Setting::UnitManager::Init();

      // Get some action defined in the "MenuManager.xml" file
      Setting::MenuManager& menuManager = Core::Singleton<Setting::MenuManager>::Get();
      QAction* actionShowData    =  menuManager.GetQAction("ShowData");
      QAction* actionSaveData    =  menuManager.GetQAction("SaveData");
      QAction* actionAbout       =  menuManager.GetQAction("About");
      QAction* actionHelp        =  menuManager.GetQAction("Help");
      QAction* actionHelpViewer  =  menuManager.GetQAction("HelpViewer");

      QObject::connect(actionNew, SIGNAL(triggered()), this, SLOT(New()));
      QObject::connect(actionUndo, SIGNAL(triggered()), this, SLOT(Undo()));
      QObject::connect(actionRedo, SIGNAL(triggered()), this, SLOT(Redo()));
      QObject::connect(actionSaveAs, SIGNAL(triggered()), this, SLOT(SaveAs()));
      QObject::connect(actionSave, SIGNAL(triggered()), this, SLOT(Save()));
      QObject::connect(actionLoad, SIGNAL(triggered()), this, SLOT(Load()));
      QObject::connect(actionQuit, SIGNAL(triggered()), this, SLOT(close()));
      QObject::connect(actionSettings, SIGNAL(triggered()), this, SLOT(ShowSetting()));
      QObject::connect(actionShowData, SIGNAL(triggered()), this, SLOT(ShowData()));
      QObject::connect(actionSaveData, SIGNAL(triggered()), this, SLOT(SaveData()));
      QObject::connect(actionAbout, SIGNAL(triggered()), this, SLOT(ShowAbout()));
      QObject::connect(actionHelp, SIGNAL(triggered()), this, SLOT(ShowHelp()));
      QObject::connect(actionHelpViewer, SIGNAL(triggered()), viewer_, SLOT(help()));

      
      UpdateHistoric();
      BuildExampleMenu();
      
      Core::System::Get().Update();
      
      //Begin a system sample
      // Item::Body& b1 = *new Item::Body();
      // Item::Body& b2 = *new Item::Body();
      // b2.ToggleFix();
      // new Item::LinkT<Item::Motor>(b1, b2);

    }
  
    MainWindow::~MainWindow()
    {
      Core::System::Quit();
      me_ = 0;   
      delete viewer_;
      delete treeView_;
    }


    TreeView& 
    MainWindow::GetTreeView()
    {
      return *treeView_;
    }

    const TreeView& 
    MainWindow::GetTreeView() const
    {
      return *treeView_;
    }

    Viewer& 
    MainWindow::GetViewer()
    {
      return *viewer_;
    }

    const Viewer& 
    MainWindow::GetViewer() const
    {
      return *viewer_;
    }



    int
    MainWindow::GetIconSize()
    {
      return iconSize_;
    }


    void
    MainWindow::AddGlobalSetting(Core::GlobalSetting& globalSetting)
    {
      globalSet_.AddItem(globalSetting);
    }
    
    void
    MainWindow::RemoveGlobalSetting(Core::GlobalSetting& globalSetting)
    {
      globalSet_.RemoveItem(globalSetting);
    }

    bool
    MainWindow::CloseDialog(bool allowCancellation)
    {
      if (currentDialog_==0)
	return true;
      else 
	{
	  if (!currentDialog_->IsPossibleToClose())
	    return false;
	  
	  if (currentDialog_->Check())
	    {
	      currentDialog_->Ok();
	      toolBox_->removeItem(1);
	      OMC_ASSERT_MSG(currentDialog_ ==0, "The current dialog must be set to zero");
	      return true;
	    }
	  else if (allowCancellation)
	    {
	      currentDialog_->Cancel();
	      toolBox_->removeItem (1);
	      OMC_ASSERT_MSG(currentDialog_ ==0, "The current dialog must be set to zero");
	      return true;
	    }
	}
      return false;
    }

    void
    MainWindow::DialogContainerIsBusy(Dialog& dial)
    {
      if (currentDialog_!=0)
	{
	  if (currentDialog_->Check())
	    currentDialog_->Ok();
	  else
	    currentDialog_->Cancel();
	  
	  toolBox_->removeItem (1);
	}
      OMC_ASSERT_MSG(currentDialog_ ==0, "The current dialog must be set to zero");
      currentDialog_ = &dial;
    }

    void
    MainWindow::DialogContainerIsFree(Dialog& dial)
    {
      OMC_ASSERT_MSG(currentDialog_ == &dial, "The current dialog must be equal the dial");
      currentDialog_ = 0;
      toolBox_->removeItem (1);
      toolBox_->setCurrentIndex(0);
    }

    QToolBox&
    MainWindow::GetDialogContainer()
    {
      return *toolBox_;
    }

    void
    MainWindow::AddToHistoric(QAction& action)
    {
      userActions_.push_back(&action);
      UpdateHistoric();
    }

    void 
    MainWindow::Undo()
    {
      if (!CloseDialog(true))
	return;
      
      Core::System::Get().GetHistoric().LoadPreviousState();
      UpdateHistoric();
    }

    void 
    MainWindow::Redo()
    {
      if (!CloseDialog(true))
	return;

      Core::System::Get().GetHistoric().LoadNextState();
      UpdateHistoric();
    }

    
    void 
    MainWindow::UpdateHistoric()
    {
      const bool undo = Core::System::Get().GetHistoric().IsUndoPossible();
      const bool redo = Core::System::Get().GetHistoric().IsRedoPossible();
      actionUndo->setEnabled(undo);
      actionRedo->setEnabled(redo);
    }

    
    void 
    MainWindow::SaveAs()
    {
      if (!CloseDialog(true))
	return;

      //the qt QFileDialog::getSaveFileName static method don't manage suffix
      //it's boring
      QFileDialog fileDialog(this, tr("Save As"));
      fileDialog.setFilter (QDir::Files);
      fileDialog.setDefaultSuffix ("omc");
      fileDialog.setFileMode(QFileDialog::AnyFile);
      fileDialog.setAcceptMode(QFileDialog::AcceptSave);
      QString fileName;
      if (fileDialog.exec()) 
	{ 
	  QStringList fileNames = fileDialog.selectedFiles(); 
	  if ( fileNames.count() ) 
	    fileName = fileNames.at(0); 
	}

      
      if (!fileName.isEmpty())
	{
	  OMC_ASSERT_MSG(fileName.endsWith(".omc"), "The .omc extension must be respected");
	  currentFileName_ = fileName.toStdString();
	  Save();
	}
    }

    
    void 
    MainWindow::New()
    {
      if (!CloseDialog(true))
	return;

      if (Core::System::Get().IsEdited())
	{
	  const QString msg = tr("The current action will clear your system, do you want to continue ?");
	  
	  QMessageBox mBox(QMessageBox::Question, tr("Are you sure ?"), 
			   msg, QMessageBox::Yes|QMessageBox::No);
	  if (QMessageBox::No == mBox.exec()) 
	    return;
	}
      Core::System::Get().Reset();
    }

    void 
    MainWindow::Save()
    {
      if (currentFileName_.empty())
	{
	  SaveAs();
	  return;
	}
      QString qfileNameStr = currentFileName_.c_str();
      OMC_ASSERT_MSG(qfileNameStr.endsWith(".omc"), "The .omc extension must be respected");
      Core::System::Get().SaveFile(currentFileName_);
    }
    
    
    void 
    MainWindow::Load()
    {
      if (!CloseDialog(true))
	return;

      
      //Todo check if system is edited
      QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),"", tr("OpenMeca Files (*.omc)"));
      if (!fileName.isEmpty())
	{
	  currentFileName_ = fileName.toStdString();
	  Core::System::Get().LoadFile(currentFileName_);
	  UpdateHistoric();
	}
     
    }

    void 
    MainWindow::showEvent(QShowEvent* event ) 
    {
      QWidget::showEvent(event);
      if (init_)
	{
	  // This is a trick because if the Update() method 
	  // is called in the constructor it bugs.
	  // This is a Qt problem, not an openmeca problem.
	  Setting::Scene::Init();
	  Core::SystemSettingT<Setting::Scene>::Get().Update();
	  init_ = false;
	  if (openFileNameQueryAtStart_ != "")
	    {
	      Core::System::Get().LoadFile(openFileNameQueryAtStart_);
	      openFileNameQueryAtStart_ = "";
	    }
	}
    }
    


    void 
    MainWindow::closeEvent(QCloseEvent* event)
    {
      Core::SystemSettingT<Setting::Simulation>::Get().Stop();
      Core::SystemSettingT<Setting::Simulation>::Get().wait();
      
      if (!CloseDialog(true))
	{
	  event->ignore();
	  return;
	}
	  
		       
      if (Core::System::Get().IsEdited())
	{
	  QMessageBox msgBox;
	  msgBox.setText(tr("The document has been modified."));
	  msgBox.setInformativeText(tr("Do you want to save your changes?"));
	  msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
	  msgBox.setDefaultButton(QMessageBox::Save);
	  int ret = msgBox.exec();
	  switch (ret) 
	  {
	  case QMessageBox::Save:
	    event->accept();
	    Save();
	    break;
	  case QMessageBox::Discard:
	    event->accept();
	    break;
	  case QMessageBox::Cancel:
	    event->ignore();
	    return;
	    break;
	  default:
	    OMC_ASSERT_MSG(0, "The default case is not allowed");
	    break;
	  }
	}
      Core::System::Get().Reset();
      
    }
    
    void 
    MainWindow::ShowData()
    {
      plot_->show(); 
    }    

    void 
    MainWindow::SaveData()
    {
      //the qt QFileDialog::getSaveFileName static method don't manage suffix
      //it's boring
      QFileDialog fileDialog(this, tr("Save data as"));
      fileDialog.setFilter(QDir::Files);
      fileDialog.setDefaultSuffix ("txt");
      fileDialog.setFileMode(QFileDialog::AnyFile);
      fileDialog.setAcceptMode(QFileDialog::AcceptSave);
      QString fileName;
      if (fileDialog.exec()) 
	{ 
	  QStringList fileNames = fileDialog.selectedFiles(); 
	  if ( fileNames.count() ) 
	    fileName = fileNames.at(0); 
	}

      
      if (!fileName.isEmpty())
	{
	  OMC_ASSERT_MSG(fileName.endsWith(".txt"), "The .txt extension must be respected");
	  Item::Physical::DumpAllDataToFile(fileName.toStdString());
	}
    }

    void 
    MainWindow::ShowAbout()
    {
      const QString version = QString::number(Core::System::Version,'f', 1);
      QByteArray byteArray;
      QBuffer buffer(&byteArray);
      QPixmap pixmap(":/Rsc/Img/Logo.png");
      pixmap.save(&buffer, "PNG");
      QString img = QString("<p align=\"center\"> <img src=\"data:image/png;base64,");
      img += byteArray.toBase64() + "\"/> </p>";

      QString text;
      text += "<h2>" + tr("Welcome to openmeca !") + "</h2>";
      text += tr("Openmeca is an easy to use software that helps you to build mechanical systems and simulate it");
      text += "<br>";
      text += img;
      text += tr("OpenMeca is distributed under the free GPL v3 license");
      text += "<br>";
      text += tr("It means that you are free to use and to distribute it !");
      text += "<br><br>";

      text += "<b>" + tr("Acknowledgement") + "</b>";
      text += "<br>";
      text += "openmeca is directly based on the <a href=\"http://chronoengine.info\">ChronoEngine library</a>, the <a href=\"http://www.boost.org\">Boost library</a>, the <a href=\"http://qwt.sf.net\">Qwt library</a>, the <a href=\"http://libqglviewer.com\">QGLViewer library</a> and a lot of others. Many thanks to their authors !"; 
      text += "<br><br>";

      
      text += "<b>" + tr("Main informations") + "</b>";
      text += Core::Software::Get().HtmlText();
	

      QMessageBox::about(this, tr("About openmeca"), text);
      
    }

    void 
    MainWindow::ShowHelp()
    {
      OMC_ASSERT_MSG(helpWindow_ != 0, "The help window pointer must not be null");
      helpWindow_->show();
    }
    
    void 
    MainWindow::BuildHelpWindow()
    {
      // use config file to ensure that the files exist in the config file dir
      // use RestoreBackup() to force their update
      Core::ConfigFile qhc_file("Help.qhc", "Help");
      qhc_file.RestoreBackup();
      qhc_file.Open(); 

      Core::ConfigFile qch_file("Help.qch", "Help");
      qch_file.RestoreBackup();
      qch_file.Open();

      QHelpEngine* helpEngine = new QHelpEngine(qhc_file.GetAbsPath());
      
      helpEngine->setupData();
      
      QTabWidget* tWidget = new QTabWidget;
      tWidget->setMaximumWidth(200);
      tWidget->addTab(helpEngine->contentWidget(), "Contents");
      tWidget->addTab(helpEngine->indexWidget(), "Index");
      
      HelpBrowser *textViewer = new HelpBrowser(helpEngine);
      textViewer->setSource(QUrl("qthelp://OpenMeca/Help/Help.html"));
      QObject::connect(helpEngine->contentWidget(),  SIGNAL(linkActivated(QUrl)),
		       textViewer, SLOT(setSource(QUrl)));
      
      QObject::connect(helpEngine->indexWidget(), SIGNAL(linkActivated(QUrl, QString)), 
		       textViewer, SLOT(setSource(QUrl)));
      
      QSplitter *horizSplitter = new QSplitter(Qt::Horizontal);
      horizSplitter->insertWidget(0, tWidget);
      horizSplitter->insertWidget(1, textViewer);
      horizSplitter->hide();
      
      helpWindow_ = new QDockWidget(tr("Help"), this);
      helpWindow_->setWidget(horizSplitter);
      helpWindow_->hide();
      addDockWidget(Qt::TopDockWidgetArea, helpWindow_);
    }

    void 
    MainWindow::BuildExampleMenu()
    {
      QStringList list = QDir(":/Rsc/Example/").entryList(QDir::Files);
      for (int i = 0; i < list.size(); ++i)
	{
	  QAction* action = new ExampleAction(list[i], menuExample);
	  menuExample->addAction(action);
	}
      
    }

    void 
    MainWindow::LoadExample()
    {
      // It does nothing, remove this ?
    }

    
    
    void 
    MainWindow::ShowSetting()
    {
      setting_->show();
    }


    const QIcon& 
    MainWindow::GetIcon() const
     {
       return icon_;
      }		 
  



  }
}



