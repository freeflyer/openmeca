// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <QMouseEvent>

#include "OpenMeca/Gui/TreeView.hpp"
#include "OpenMeca/Gui/AbstractTreeItem.hpp"
#include "OpenMeca/Core/SelectionManager.hpp"
#include "OpenMeca/Core/CommonProperty.hpp"

namespace OpenMeca
{
  
  namespace Gui
  {
    TreeView::TreeView(QWidget *parent )
      :QTreeWidget(parent)
    {
      AbstractTreeItem::SetTreeView(*this);
      QObject::connect(this, SIGNAL(itemSelectionChanged()), this, SLOT(ItemSelect()));
    }
    
    TreeView::~TreeView()
    {

    }
    
    void 
    TreeView::AddAbstractTreeItem(AbstractTreeItem& treeItem)
    {
      treeItems_.AddItem(treeItem);
    }

    void 
    TreeView::EraseAbstractTreeItem(AbstractTreeItem& treeItem)
    {
      treeItems_.RemoveItem(treeItem);
    }

    void 
    TreeView::ItemSelect()
    {
      QList<QTreeWidgetItem *> list = selectedItems ();
      if (list.size()==0)
	{
	  return;
	}
      OMC_ASSERT_MSG(list.size()==1, "The selected item list size must contoin one element");
      AbstractTreeItem* treeItem = ConvertToAbstractTreeItem(list[0]);
      if (treeItem != 0)
	Core::Singleton<Core::SelectionManager>::Get().SetItemSelected(treeItem->GetItem());
      else
	Core::Singleton<Core::SelectionManager>::Get().SetNoItemSelected();
    }

    AbstractTreeItem*
    TreeView::ConvertToAbstractTreeItem(QTreeWidgetItem* qItem)
    {
      AbstractTreeItem* treeItem = static_cast<AbstractTreeItem*>(qItem);
      if (treeItems_.Contain(treeItem))
	return treeItem;
      else
	return 0;
    }

    void TreeView::mousePressEvent(QMouseEvent* e)
    {
      QTreeWidgetItem* qItem = itemAt(e->pos());
      AbstractTreeItem* treeItem = ConvertToAbstractTreeItem(qItem);
      if (treeItem != 0)
      	{
      	  if (e->button() == Qt::RightButton)
      	    {
      	      Core::Singleton<Core::SelectionManager>::Get().SetItemSelected(treeItem->GetItem());
      	      Core::CommonProperty::GetClass(treeItem->GetItem().GetStrType()).GetPopUpMenu().popup(mapToGlobal(e->pos()));
	      return;
      	    }
      	}
      QTreeWidget::mousePressEvent(e);
    }
    
  }
}
