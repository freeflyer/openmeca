// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <QApplication>
#include <iostream>

#include "OpenMeca/Gui/Prop/PropMotionLaw.hpp"
#include "OpenMeca/Gui/Prop/PropTree.hpp"
#include "OpenMeca/Util/Dimension.hpp"
#include "OpenMeca/Util/Unit.hpp"

namespace OpenMeca
{
  namespace Gui
  {

    PropMotionLaw::PropMotionLaw(QWidget* parent)
      :PropT<Util::MotionLaw>(*parent),
       itemCheckBox_(),
       itemWidgetExpr_(),
       checkBox_(),
       widgetExpr_(0)
    {
      AddSubItem(&itemCheckBox_);
      AddSubItem(&itemWidgetExpr_);
    }

    PropMotionLaw::~PropMotionLaw()
    {
    }

    
    void 
    PropMotionLaw::Insert(PropTree& tree)
    {
      tree.addTopLevelItem(&item_);
      item_.setText(0, Prop::GetLabel());
      
      item_.addChild(&itemCheckBox_);
      itemCheckBox_.setText(0, QObject::tr("Enabled"));
      tree.setItemWidget(&itemCheckBox_,1, &checkBox_);

      item_.addChild(&itemWidgetExpr_);
      itemWidgetExpr_.setText(0, QObject::tr("Motion Law"));
      tree.setItemWidget(&itemWidgetExpr_,1, &widgetExpr_);
    }

    void 
    PropMotionLaw::SetDimension(const Util::Dimension& dim)
    {
      widgetExpr_.SetDimension(dim);
    }


    const Util::Dimension&
    PropMotionLaw::GetDimension() const
    {

      
      return widgetExpr_.GetDimension();
    }


    void 
    PropMotionLaw::Init()
    {
      const bool value =  PropT<Util::MotionLaw>::GetCopy().IsEnabled();
      if (value)
	checkBox_.setCheckState(Qt::Checked);
      else
	checkBox_.setCheckState(Qt::Unchecked);

      widgetExpr_.SetExpr(PropT<Util::MotionLaw>::GetCopy().GetLaw());
    }
    
    void 
    PropMotionLaw::DisplayHelp(const QString& message)
    {
      widgetExpr_.DisplayHelp(message);
    }

   
    bool
    PropMotionLaw::Check()
    {
     
      if (checkBox_.checkState() == Qt::Checked)
	PropT<Util::MotionLaw>::GetCopy().GetEnabled() = true;
      else if (checkBox_.checkState() == Qt::Unchecked)
	PropT<Util::MotionLaw>::GetCopy().GetEnabled() = false;
      else
	OMC_ASSERT_MSG(0, "This case is forbidden");

      return (widgetExpr_.GetExpr(PropT<Util::MotionLaw>::GetCopy().GetLaw()));
    }


  }
}


