// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QApplication>
#include <QToolTip>
#include <QWidget>
#include <iostream>

#include "OpenMeca/Gui/Prop/PropExpr.hpp"
#include "OpenMeca/Gui/Prop/PropTree.hpp"
#include "OpenMeca/Util/Unit.hpp"

namespace OpenMeca
{
  namespace Gui
  {

    PropExpr::PropExpr(QWidget* parent)
      :PropT<Util::Expr>(*parent),
       widgetExpr_(0)
    {
    }

    PropExpr::~PropExpr()
    {
    }

    void 
    PropExpr::Insert(PropTree& tree)
    {
      tree.addTopLevelItem(&item_);
      item_.setText(0, Prop::GetLabel());
      tree.setItemWidget(&item_,1, &widgetExpr_);
    }

    void 
    PropExpr::SetDimension(const Util::Dimension& dim)
    {
      widgetExpr_.SetDimension(dim);
    }

    const Util::Dimension&
    PropExpr::GetDimension() const
    {
      return widgetExpr_.GetDimension();
    }
    
    
    void 
    PropExpr::Init()
    {
      widgetExpr_.SetExpr(PropT<Util::Expr>::GetValue());
    }
   
    bool
    PropExpr::Check()
    {
      return (widgetExpr_.GetExpr(PropT<Util::Expr>::GetCopy()));
    }

    void 
    PropExpr::DisplayHelp(const QString& message)
    {
      widgetExpr_.DisplayHelp(message);
    }

    void 
    PropExpr::AddCondition(const Core::Condition<double>* cond)
    {
      widgetExpr_.AddCondition(cond);
    }

    void 
    PropExpr::PostChangement()
    {
      PropT<Util::Expr>::GetValue().Update();
    }

  }
}


