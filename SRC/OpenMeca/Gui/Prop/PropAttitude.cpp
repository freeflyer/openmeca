// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <QApplication>
#include <iostream>

#include "OpenMeca/Gui/Prop/PropAttitude.hpp"
#include "OpenMeca/Gui/Prop/PropTree.hpp"
#include "OpenMeca/Util/Dimension.hpp"
#include "OpenMeca/Util/Unit.hpp"

namespace OpenMeca
{
  namespace Gui
  {
    PropAttitude::PropAttitude(QWidget* parent)
      :PropT<Geom::Quaternion<_3D> >(*parent),
       itemAxis_(),
       itemX_(),
       itemY_(),
       itemZ_(),
       itemA_(),
       x_(0),
       y_(0),
       z_(0),
       a_(0)
    {
      AddSubItem(&itemAxis_);
      AddSubItem(&itemX_);
      AddSubItem(&itemY_);
      AddSubItem(&itemZ_);
      AddSubItem(&itemA_);

      x_.SetDimension(Util::Dimension::Get("Length"));
      y_.SetDimension(Util::Dimension::Get("Length"));
      z_.SetDimension(Util::Dimension::Get("Length"));
      a_.SetDimension(Util::Dimension::Get("Angle"));
    }

    PropAttitude::~PropAttitude()
    {
    }

    void 
    PropAttitude::Insert(PropTree& tree)
    {
      tree.addTopLevelItem(&item_);
      item_.setText(0, Prop::GetLabel());
      
      item_.addChild(&itemAxis_);
      itemAxis_.setText(0, "Axis");

      itemAxis_.addChild(&itemX_);
      itemX_.setText(0, "X");
      tree.setItemWidget(&itemX_,1, &x_);

      itemAxis_.addChild(&itemY_);
      itemY_.setText(0, "Y");
      tree.setItemWidget(&itemY_,1, &y_);

      itemAxis_.addChild(&itemZ_);
      itemZ_.setText(0, "Z");
      tree.setItemWidget(&itemZ_,1, &z_);

      item_.addChild(&itemA_);
      itemA_.setText(0, "Angle");
      tree.setItemWidget(&itemA_,1, &a_);

    }

    void 
    PropAttitude::Init()
    {
      const Geom::Quaternion<_3D>& q =  PropT<Geom::Quaternion<_3D> >::GetValue();
      Geom::Vector<_3D> v = q.GetAxis();
      if (v.GetNorm() == 0.)
	v[0] = 1.;
      const double a = q.GetAngle();
      x_.SetNumber(v[0]);
      y_.SetNumber(v[1]);
      z_.SetNumber(v[2]);
      a_.SetNumber(a);
    }
    

    bool
    PropAttitude::Check()
    {
      double x, y, z, a;
      bool ok = (x_.GetValue(x) && y_.GetValue(y) && z_.GetValue(z) && a_.GetValue(a));
      if (ok == false)
	return false;

      Geom::Vector<_3D>v(x,y,z);
      if (v.GetNorm()==0.)
	{
	  QString msg(QObject::tr("The axis can't be null"));
	  x_.DisplayHelp(msg);
	  return false;
	}
      Geom::Quaternion<_3D> q(Geom::Vector<_3D>(x,y,z), a);
      PropT<Geom::Quaternion<_3D> >::GetCopy() = q;
      return true;;
    }

  }
}

