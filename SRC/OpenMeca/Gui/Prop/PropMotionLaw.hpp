// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Prop_PropMotionLaw_hpp
#define OpenMeca_Prop_PropMotionLaw_hpp


#include <QLabel>
#include <QCheckBox>

#include "OpenMeca/Gui/Prop/PropT.hpp"
#include "OpenMeca/Gui/Widget/WidgetExpr.hpp"
#include "OpenMeca/Util/Dimension.hpp"

#include "OpenMeca/Util/MotionLaw.hpp"



namespace OpenMeca
{
  namespace Gui
  {
    class PropMotionLaw: public PropT<Util::MotionLaw>
    {
      public:
      PropMotionLaw(QWidget* parent);
      virtual ~PropMotionLaw();

      void Insert(PropTree&);
      
      void SetDimension(const Util::Dimension& dim);
      const Util::Dimension& GetDimension() const;
      
      bool Check();
      void ApplyValue();

      void DisplayHelp(const QString& message);

    private:
      void Init();

    private:
      QTreeWidgetItem itemCheckBox_;
      QTreeWidgetItem itemWidgetExpr_;
      QCheckBox checkBox_;
      WidgetExpr widgetExpr_;
    };

  }
}
#endif

