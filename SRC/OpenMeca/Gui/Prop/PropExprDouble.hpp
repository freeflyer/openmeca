// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Prop_PropExprDouble_hpp
#define OpenMeca_Prop_PropExprDouble_hpp


#include <QLabel>
#include <QLineEdit>

#include "OpenMeca/Gui/Prop/PropT.hpp"
#include "OpenMeca/Gui/Widget/WidgetExpr.hpp"
#include "OpenMeca/Util/Dimension.hpp"

#include "OpenMeca/Util/ExprDouble.hpp"


namespace OpenMeca
{
  namespace Gui
  {
    class PropExprDouble: public PropT<Util::ExprDouble>
    {
      public:
      PropExprDouble(QWidget* parent);
      virtual ~PropExprDouble();

      void Insert(PropTree&);
      
      void SetDimension(const Util::Dimension& dim);
      const Util::Dimension& GetDimension() const;
      
      bool Check();
      void ApplyValue();

      void AddComponentCondition(const Core::Condition<double>*);
      void PostChangement();

    private:
      void Init();

    private:
      QTreeWidgetItem w_item_;
      WidgetExpr val_;
 
   
    };


 

    

  }
}
#endif

