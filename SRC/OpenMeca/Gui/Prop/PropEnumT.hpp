// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Prop_PropEnumT_hpp
#define OpenMeca_Prop_PropEnumT_hpp


#include <QWidget>
#include <QBoxLayout>
#include <QLabel>
#include <QComboBox>

#include "OpenMeca/Util/Enum.hpp"
#include "OpenMeca/Gui/Prop/PropT.hpp"
#include "OpenMeca/Gui/Widget/WidgetEnum.hpp"
#include "OpenMeca/Gui/Prop/PropTree.hpp"

namespace OpenMeca
{
  namespace Gui
  {
    template<typename T>
    class PropEnumT: public PropT<T>
    {
      
    public:
      PropEnumT(QWidget* parent);
      virtual ~PropEnumT();

      void Insert(PropTree&);

      bool Check();
      void ApplyValue();

    private:
      void UpdateComboBox();
      void Init();   

    private:
      WidgetEnum enum_;
      bool isInit_;
    };


    template<typename T>
    inline
    PropEnumT<T>::PropEnumT(QWidget* parent)
      :PropT<T>(*parent),
       enum_(parent),
       isInit_(false)
    {
      const std::vector<std::string> str = Util::Enum<T>::List();
      for (unsigned int i = 0; i < str.size(); ++i)
	enum_.addItem(str[i].c_str());
    }

    template<typename T>
    inline
    PropEnumT<T>::~PropEnumT()
    {
    }

    template<typename T>
    inline void 
    PropEnumT<T>::Insert(PropTree& tree)
    {
      QTreeWidgetItem& item = Prop::item_;
      tree.addTopLevelItem(&item);
      item.setText(0, Prop::GetLabel());
      tree.setItemWidget(&item,1, &enum_);
    }

    template<typename T>
    inline void 
    PropEnumT<T>::Init()
    {
      const std::string value = Util::Enum<T>::ToString(PropT<T>::GetValue());
      enum_.setCurrentText(value.c_str());
    }
    
    template<typename T>
    inline bool
    PropEnumT<T>::Check()
    {
      const std::string str = enum_.currentText().toStdString();
      PropT<T>::GetCopy() = Util::Enum<T>::ToEnum(str);
      return true;
    }

  }
}
#endif

