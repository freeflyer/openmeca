// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Prop_PropDouble_hpp
#define OpenMeca_Prop_PropDouble_hpp


#include <QLabel>
#include <QLineEdit>

#include "OpenMeca/Gui/Prop/PropT.hpp"
#include "OpenMeca/Gui/Widget/WidgetDouble.hpp"
#include "OpenMeca/Core/Condition.hpp"

namespace OpenMeca
{
  namespace Gui
  {
    
    class PropDouble: public PropT<double>
    {
      
    public:
      PropDouble(QWidget* parent);
      virtual ~PropDouble();
      
      void Insert(PropTree&);

      void SetDimension(const Util::Dimension& dim);
      const Util::Dimension& GetDimension() const;
      
      bool Check();
      void ApplyValue();

      void DisplayHelp(const QString& message);
      void AddCondition(const Core::Condition<double>* cond);

      QString GetInputString() const;

    private:
      void Init();

    private:
      WidgetDouble widgetDouble_;
    };

  }
}
#endif

