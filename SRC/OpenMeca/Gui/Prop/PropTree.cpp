// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Gui/Prop/PropTree.hpp"
#include "OpenMeca/Gui/Dialog/Dialog.hpp"
#include "OpenMeca/Gui/Prop/Prop.hpp"

namespace OpenMeca
{
  namespace Gui
  {

  
    PropTree::PropTree(QWidget* parent)
      :QTreeWidget(parent),
       init_(false)
    {
     setColumnCount(2);
     QStringList list = (QStringList() << QObject::tr("Property") << QObject::tr("Value"));
     setHeaderLabels(list);
     setStyleSheet("QLineEdit{border: none;background:transparent;} QLineEdit:focus {background:white;}");
    }

    
    PropTree::~PropTree()
    {
    }

    void 
    PropTree::Init()
    {
      setColumnCount(2);
      QStringList list = (QStringList() << QObject::tr("Property") << QObject::tr("Value"));
      setHeaderLabels(list);
      init_ = true;
    }

    void 
    PropTree::Add(Prop& prop)
    {
      if (!init_)
	Init();

      prop.Insert(*this);

      if (topLevelItemCount() % 2 == 0)
	prop.SetColor(QColor(255, 255, 180));
      else
	prop.SetColor(QColor(255, 255, 200));
    }

  
  }
}
