// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef _OpenMeca_Physic_Double_hpp_
#define _OpenMeca_Physic_Double_hpp_

#include <QObject>
#include <QMenu>
#include <QTreeWidget>
#include "OpenMeca/Physic/TypeT.hpp"
#include "OpenMeca/Physic/Quantity.hpp"
#include "OpenMeca/Geom/Vector.hpp"
#include "OpenMeca/Util/Unit.hpp"


namespace OpenMeca
{
  namespace Physic
  {

    class Double : public QObject, public TypeT <double >
    {
      Q_OBJECT
      
    public:
      Double(Quantity&);
      ~Double();

      void Update(const double&);
      void Update();

      void SaveState();
      void ResetState();
      void RecoveryState(unsigned int);
      double& GetRealType();
      const double& GetRealType() const;
			       
      void FillDataTree(QTreeWidgetItem*);
      void UpdateDataTree();

      void WriteHeaderDataFile(std::ofstream&);
      void WriteDataFile(std::ofstream&, unsigned int);	

      void Draw() const;

						      
    private slots:
      void Val(bool);

    private:
      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive & ar, const unsigned int);
    
    private:
      double val_;
      std::vector<double> storedVal_;
      QTreeWidgetItem* valItem_;

    };


    template<class Archive>
    inline void
    Double::serialize(Archive & ar, const unsigned int)
    {
	ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(TypeT<double>);
	ar & BOOST_SERIALIZATION_NVP(val_);
    }

  }
} 

namespace boost 
{ 
  namespace serialization 
  {
    
    template<class Archive>
    inline void save_construct_data(Archive & ar, 
				    const OpenMeca::Physic::Double * t, 
				    const unsigned int)
    {
      const OpenMeca::Physic::Quantity* parent = &t->GetQuantity();
      ar << parent;
    }
    

    template<class Archive>
    inline void load_construct_data(Archive & ar, 
				    OpenMeca::Physic::Double * t, 
				    const unsigned int)
    {
      OpenMeca::Physic::Quantity* parent = 0;
      ar >> parent;
      ::new(t)OpenMeca::Physic::Double(*parent);
    }
  }
} // namespace ...


#endif
