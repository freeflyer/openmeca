// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QWidget>

#include "OpenMeca/Physic/Torque.hpp"
#include "OpenMeca/Util/Dimension.hpp"
#include "OpenMeca/Core/System.hpp"

#include <boost/serialization/export.hpp>
//Don't forget to export for dynamic serialization of child class
BOOST_CLASS_EXPORT(OpenMeca::Physic::Torque)

namespace OpenMeca
{  
  namespace Physic
  {

    const std::string
    Torque::GetStrType()
    {
      return "Torque";
    }

    const QString
    Torque::GetQStrType()
    {
      return QObject::tr("Torque");
    }
      

    Torque::Torque(Item::Physical& item)
      :MechanicalAction(item, Util::Color::Red),
       unit_(Util::Dimension::Get("Torque").GetUnit("NewtonPerMeter"))
    {
      x_.SetDimension(Util::Dimension::Get("Torque"));
      y_.SetDimension(Util::Dimension::Get("Torque"));
      z_.SetDimension(Util::Dimension::Get("Torque"));
    }
   
    Torque::~Torque()
    {
    }

 
    const Util::Unit& 
    Torque::GetUnit() const
    {
      return unit_;
    }
    
    double
    Torque::GetScale() const
    {
      return Core::System::Get().GetScales().GetScaleValue(Torque::GetStrType());
    }

    void
    Torque::CompleteChForce(chrono::ChSharedForcePtr& chTorque)
    {
      chTorque->SetMode(FTYPE_TORQUE);
    }

  }
}
