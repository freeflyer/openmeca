// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Physic/Double.hpp"
#include "OpenMeca/Gui/MainPlotWindow.hpp"

#include <QPushButton>
#include <functional>
#include <boost/bind.hpp>
#include <boost/serialization/export.hpp>
//Don't forget to export for dynamic serialization of child class
BOOST_CLASS_EXPORT(OpenMeca::Physic::Double)

namespace OpenMeca
{
  namespace Physic
  {

    Double::Double(Quantity& q)
      :TypeT<double>(q),
       val_(0.), 
      storedVal_(),
      valItem_(0)
    {
    }
    
    Double::~Double()
    {
    }

    double& 
    Double::GetRealType()
    {
      return val_;
    }
    
    const double& 
    Double::GetRealType() const
    {
      return val_;
    }
    
    void
    Double::SaveState()
    {
      Update(val_);
    }

    void
    Double::Update()
    {
      storedVal_.push_back(0.);
    }


    void
    Double::Update(const double& v)
    {
      val_ = v;
      storedVal_.push_back(v);
    }

    void
    Double::ResetState()
    {
      storedVal_.clear();
    }

    void
    Double::RecoveryState(unsigned int i)
    {
      val_ = storedVal_[i];
    }


    void 
    Double::FillDataTree(QTreeWidgetItem* tree)
    {
      dataTreeItem_ = tree;

      valItem_ =  new QTreeWidgetItem(tree);
      valItem_->setText(0, "Val");
      UpdateDataTree();

      auto button = new QPushButton(plusIcon_, "");
      button->setCheckable(true);
      button->setMaximumSize(20, button->height());
      tree->treeWidget()->setItemWidget(valItem_, 2, button);

      QObject::connect(button, SIGNAL(toggled(bool)), this, SLOT(Val(bool)));
      tree->setExpanded(true);
    }

    void 
    Double::UpdateDataTree()
    {
      OMC_ASSERT_MSG(valItem_!=0, "DataTreeItem has not been instantiate !");
      
      const QString unit = QString(" ") + GetUnit().GetSymbol().c_str();
      if (storedVal_.size() > 0)
	valItem_->setText(1,QString::number(storedVal_.back()) + unit);
    }

    
    void 
    Double::Val(bool val)
    {
      if (val)
	{
	  std::string label = GetLabel();
	  Gui::MainPlotWindow::Get().AddData(storedVal_, label.c_str(), GetUnit());
	}
      else
	Gui::MainPlotWindow::Get().RemoveData(storedVal_);
    }

   
    void 
    Double::WriteHeaderDataFile(std::ofstream& file)
    {
      file << GetLabel() << "(" <<  GetUnit().GetSymbol() << ")";
    }

    void 
    Double::WriteDataFile(std::ofstream& file, unsigned int iter)
    {
      OMC_ASSERT_MSG(iter < storedVal_.size(), "The data are not coherent");
      file << storedVal_[iter];
    }

    void 
    Double::Draw() const
    {
      //Nothing
    }


  

  }
} 

