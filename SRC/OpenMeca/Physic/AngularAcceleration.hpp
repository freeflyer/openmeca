// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Physic_AngularAcceleration_hpp
#define OpenMeca_Physic_AngularAcceleration_hpp

#include "OpenMeca/Physic/QuantityT.hpp"
#include "OpenMeca/Physic/Vector3D.hpp"
#include "OpenMeca/Item/Physical.hpp"

namespace OpenMeca
{
  namespace Physic
  {

    
    class AngularAcceleration : public QuantityT<Vector3D>
    {
    public:  
      static const Util::Color color;
      static const std::string GetStrType();
      static const QString GetQStrType();

    public:
      AngularAcceleration(Item::Physical& item);
      ~AngularAcceleration();
      
      const Util::Unit& GetUnit() const;
      double GetScale() const;
      
    private:
      friend class boost::serialization::access;
      template<class Archive> void serialize(Archive& ar, const unsigned int version);

    private:
      const Util::Unit& unit_;
    }; 

    template<class Archive>
    inline void
    AngularAcceleration::serialize(Archive& ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(QuantityT<Vector3D>);
    }
 
  }
}

namespace boost 
{ 
  namespace serialization 
  {
    
    template<class Archive>
    inline void save_construct_data(Archive & ar, 
				    const OpenMeca::Physic::AngularAcceleration * t, 
				    const unsigned int)
    {
      const OpenMeca::Item::Physical* parent = &t->GetPhysicalItem();
      ar << parent;
    }
    

    template<class Archive>
    inline void load_construct_data(Archive & ar, 
				    OpenMeca::Physic::AngularAcceleration * t, 
				    const unsigned int)
    {
      OpenMeca::Item::Physical* parent = 0;
      ar >> parent;
      ::new(t)OpenMeca::Physic::AngularAcceleration(*parent);
    }
  }
} // namespace ...


#endif
