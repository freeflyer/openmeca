// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Physic_Quantity_hpp
#define OpenMeca_Physic_Quantity_hpp

#include "OpenMeca/Util/Unit.hpp"
#include "OpenMeca/Util/Color.hpp"
#include "OpenMeca/Geom/Frame.hpp"

namespace OpenMeca
{

  namespace Item
  {
    class Physical;
  }

  namespace Physic
  {    

    class Type;

    class Quantity
    {
    public:
      Quantity(Item::Physical&, const Util::Color& color);
      virtual ~Quantity();	
      
      const std::string& GetLabel();
      
      virtual void Draw();
      virtual void BeginDraw();
      virtual void EndDraw();
      virtual const Geom::Frame<_3D>& GetFrame() const;
      virtual const Util::Unit& GetUnit() const = 0;
      virtual double GetScale() const = 0;
      virtual Type& GetDataType() = 0;
      virtual const Type& GetDataType() const = 0;

      const Util::Color& GetColor() const;
      Util::Color& GetColor();

      const Item::Physical& GetPhysicalItem() const;
      Item::Physical& GetPhysicalItem();

      virtual void Init();


    private:
      friend class boost::serialization::access;
      template<class Archive> void serialize(Archive& ar, const unsigned int version);

    private:
      Item::Physical& item_;
      Util::Color color_;
    }; 

    template<class Archive>
    inline void
    Quantity::serialize(Archive& ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_NVP(item_);
      ar & BOOST_SERIALIZATION_NVP(color_);
    }

  }
}


#endif
