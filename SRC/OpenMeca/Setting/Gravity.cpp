// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Setting/Gravity.hpp"
#include "OpenMeca/Item/Part.hpp"
#include "OpenMeca/Core/SystemSettingCommonProperty.hpp"
#include "OpenMeca/Gui/Dialog/DialogGravity.hpp"


namespace OpenMeca
{
  namespace Setting
  {

    const 
    std::string Gravity::GetStrType()
    {
      return "Gravity";
    }

    const 
    QString Gravity::GetQStrType()
    {
      return QObject::tr("Gravity");
    }

    void
    Gravity::Init()
    {
      Core::SystemSettingT< Gravity>::Get();      
      Core::Singleton< Core::SystemSettingCommonProperty<Gravity> >::Get().RegisterAction();
    }


    Gravity::Gravity()
      :Core::SystemSettingT<Gravity>(),
       g_(0., -9.81, 0.)
    {
    }

    Gravity::~Gravity()
    {
    }


    void 
    Gravity::Save(boost::archive::text_oarchive& ar, const unsigned int )
    {
      ar << BOOST_SERIALIZATION_NVP(g_);
    }

    void 
    Gravity::Load(boost::archive::text_iarchive& ar, const unsigned int ) 
    {
      ar >> BOOST_SERIALIZATION_NVP(g_);
    }

    void 
    Gravity::BuildChSystem(chrono::ChSystem& chSystem)
    {
      chSystem.Set_G_acc(g_.ToChVector());
    }

  }
}
 

