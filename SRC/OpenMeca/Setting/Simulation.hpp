// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_Simulation_hpp
#define OpenMeca_Core_Simulation_hpp

#include <QThread>
#include <map>
#include <QReadWriteLock>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include "OpenMeca/Core/SystemSettingT.hpp"
#include "OpenMeca/Core/SetOf.hpp"
#include "OpenMeca/Core/AutoRegister.hpp"

#include "ChronoEngine/physics/ChSystem.h"

namespace OpenMeca
{
  namespace Gui
  {
    class DialogSimulation; // put the #include in .cpp
  }

  namespace Setting
  {
    


    // The simulation settings
    class Simulation : public QThread,
		       public Core::SystemSettingT<Simulation>
    {
      Q_OBJECT
    
    public:
      typedef Gui::DialogSimulation Dialog;

    public:  
      static void Init();
      static const std::string GetStrType(); 
      static const QString GetQStrType(); 
      
    public:
      struct Step
      {
	
	Step():
	  time(0.), 
	  iterNumber(0),
	  totalIterNumber(0),
	  totalIterNumberAchieved(0)
	{
	}

	void Init() 
	{
	  time                    = 0.;
	  iterNumber              = 0 ;
	  totalIterNumber         = 0 ;
	  totalIterNumberAchieved = 0 ;
	}

	bool IsCleared()
	{
	  return (iterNumber == 0);
	}

	double time;
	unsigned int iterNumber;
	unsigned int totalIterNumber;
	unsigned int totalIterNumberAchieved;
      };

    public:    
      Simulation();
      virtual ~Simulation();


      void Start();
      void Pause();
      void Stop();
      void NextStep();
      void Initialize();

      double& GetTimeStep();
      const double& GetTimeStep() const ;

      double& GetTotalSimulationTime();
      const double& GetTotalSimulationTime() const ;

      chrono::ChSystem::eCh_lcpSolver& GetSolver();
      const chrono::ChSystem::eCh_lcpSolver& GetSolver() const;

      chrono::ChSystem::eCh_integrationType& GetIntegrationType();
      const chrono::ChSystem::eCh_integrationType& GetIntegrationType() const;

      void InitChSystem(chrono::ChSystem&);
      void BuildChSystem(chrono::ChSystem&);
      
      const std::vector<double>& GetTimeArray();

      QReadWriteLock& GetLock() {return lock_;};

      void Save(boost::archive::text_oarchive&, const unsigned int);
      void Load(boost::archive::text_iarchive&, const unsigned int);

      OMC_ACCESSOR(AnimationPeriod  , double , animationPeriod_   );
      OMC_ACCESSOR(StaticEquilibrium, bool   , staticEquilibrium_ );
      OMC_ACCESSOR(Step             , Step   , current_           );
      
      bool IsRunning() const;

    public slots:
      void GoToStep(unsigned int);

    signals:
      void StepChanged(OpenMeca::Setting::Simulation::Step);
      void End();

    private:
      void WaitWhileStepIsRunning();
      void run();

      Simulation(const Simulation&);            //Not Allowed    
      Simulation& operator=(const Simulation&); //Not Allowed
      
      
 

    private:
      bool initialize_;
      Step current_;
      std::map<unsigned int, double> recordTime_;
      std::vector<double> timeArray_;
      double timeStep_;
      double totalSimulationTime_;
      chrono::ChSystem::eCh_lcpSolver solver_;
      chrono::ChSystem::eCh_integrationType integrationType_;
      bool stopLoopRequired_;
      bool pauseLoopRequired_;
      double animationPeriod_;
      bool isRunning_;
      bool staticEquilibrium_;
      QReadWriteLock lock_;
    };

   


  }
}
 
#endif
