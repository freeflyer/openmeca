## This file is part of OpenMeca, an easy software to do mechanical simulation.
##
## Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
##
## Copyright (C) 2012 Damien ANDRE
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.


HEADERS += Setting/MenuManager.hpp \
           Setting/UnitManager.hpp \
           Setting/LangManager.hpp \
           Setting/Scales.hpp \
           Setting/Gravity.hpp \
           Setting/Simulation.hpp \
           Setting/Scene.hpp \


SOURCES += Setting/MenuManager.cpp \
           Setting/UnitManager.cpp \
           Setting/LangManager.cpp \
           Setting/Scales.cpp \
           Setting/Gravity.cpp \
           Setting/Simulation.cpp \
           Setting/Scene.cpp \

