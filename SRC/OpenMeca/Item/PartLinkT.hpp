// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef OpenMeca_Item_PartLinkT_hpp
#define OpenMeca_Item_PartLinkT_hpp

#include <boost/lexical_cast.hpp>

#include "OpenMeca/Item/Part.hpp"
#include "OpenMeca/Item/PartUserPoint.hpp"
#include "OpenMeca/Core/AutoRegisteredPtr.hpp"
#include "OpenMeca/Core/UserItemCommonProperty.hpp"
#include "OpenMeca/Core/None.hpp"



namespace OpenMeca
{  
  namespace Item
  {

    template <class T> class LinkT;
    // The PartLinkT is a part that belong to a link. A link
    // is drawn by two parts that belong to the two linked bodies.
    // N correspond to body (1 or 2)
    template <class T, int N>
    class PartLinkT: public Part
    {
    
    public:
      static const std::string GetStrType();
      static const QString GetQStrType();

      static void Init();

      typedef Core::None Dialog;

    public:
      PartLinkT(LinkT<T>& link);
      virtual ~PartLinkT();

      LinkT<T>& GetLinkT();
      const LinkT<T>& GetLinkT() const;

      void UpdateIcon();
      void DrawShape();
      void BeginDraw();

      Body& GetBody();
      const Body& GetBody() const;

      void Update();

      const Geom::Frame<_3D>& GetFrame() const;
      Geom::Frame<_3D>& GetFrame();

      Geom::Point<_3D>& GetCenter();
      const Geom::Point<_3D>& GetCenter() const;

      Geom::Quaternion<_3D>& GetQuaternion();
      const Geom::Quaternion<_3D>& GetQuaternion() const;

      const Geom::Frame<_3D>& GetReferenceFrame() const;

      
      const Util::Color& GetColor() const;
      Util::Color& GetColor();

      const Core::AutoRegisteredPtr<LinkT<T>, PartLinkT<T,N> >& GetLinkTPtr() const;
      Core::AutoRegisteredPtr<LinkT<T>, PartLinkT<T,N> >& GetLinkTPtr();

    private:
      PartLinkT(); //Not allowed, just for serialization
      PartLinkT(const PartLinkT<T,N>&);             //Not Allowed
      PartLinkT& operator=(const PartLinkT<T,N>&);  //Not Allowed

      QTreeWidgetItem& GetParentTreeItem();
      Core::SetOfBase<Core::UserItem> GetAssociatedSelectedItem();

      void UpdateName();

      friend class boost::serialization::access;
      template<class Archive> void save(Archive& ar, const unsigned int) const;
      template<class Archive> void load(Archive& ar, const unsigned int);
      BOOST_SERIALIZATION_SPLIT_MEMBER()


    private:
      Core::AutoRegisteredPtr<LinkT<T>, PartLinkT<T,N> > link_;
      Geom::Point<_3D> center_;
      Geom::Quaternion<_3D> quaternion_;
      Geom::Frame<_3D> frame_;
      Core::SetOf<PartPoint> points_;
    };


    template <class T, int N>  
    template<class Archive>
    inline void
    PartLinkT<T,N>::save(Archive& ar, const unsigned int) const
    {

      ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(Part);
      ar << BOOST_SERIALIZATION_NVP(points_);
    }


    template <class T, int N>  
    template<class Archive>
    inline void
    PartLinkT<T,N>::load(Archive& ar, const unsigned int) 
    {
      points_.ClearAndDelete();
      ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(Part);
      ar >> BOOST_SERIALIZATION_NVP(points_);
    }

    
    template<class T, int N> 
    inline const std::string 
    PartLinkT<T,N>::GetStrType()
    {
      return std::string(Part::GetStrType() + '_' + T::GetStrType() + '_' 
			 + boost::lexical_cast<std::string>(N));
    }

    template<class T, int N> 
    inline const QString 
    PartLinkT<T,N>::GetQStrType()
    {
      return QString(Part::GetQStrType() + '_' + T::GetQStrType() + '_' 
		     + boost::lexical_cast<std::string>(N).c_str());
    }


    template<class T, int N> 
    inline void 
    PartLinkT<T,N>::Init()
    {
      Core::Singleton< Core::UserItemCommonProperty< PartLinkT<T,N> > >::Get().CreateAction_Nothing();
    }


    template<class T, int N>
    inline
    PartLinkT<T,N>::PartLinkT(LinkT<T>& link)
      :Part(GetStrType(), link.template GetBody<N>().GetMainTreeItem(), true),
       link_(*this, link),
       center_(boost::bind(&PartLinkT<T,N>::GetReferenceFrame, boost::ref(*this))),
       quaternion_(boost::bind(&PartLinkT<T,N>::GetReferenceFrame, boost::ref(*this))),
       frame_(center_, quaternion_),
       points_()
    {      
      UpdateName();
      GetLinkT().GetLinkType().template BuildPoints<N>(points_, *this);
    }

 
    template<class T, int N>
    inline
    PartLinkT<T,N>::~PartLinkT()
    {
    }


    template<class T, int N>
    inline LinkT<T>&
    PartLinkT<T,N>::GetLinkT()
    {
      return *link_.GetPtr();
    }

    template<class T, int N>
    inline const LinkT<T>& 
    PartLinkT<T,N>::GetLinkT() const
    {
      return *link_.GetPtr();
    }

    template<class T, int N>
    inline void
    PartLinkT<T,N>::Update()
    {
      GetLinkT().GetLinkType().UpdatePart();
      UpdateName();
      Core::UserItem::Update();
      
    }


  


    template<class T, int N>
    inline const Geom::Frame<_3D>&
    PartLinkT<T,N>::GetReferenceFrame() const
    {
      return GetLinkT().template GetFrameBody<N>(); 
    }


    

    template<class T, int N>
    inline void
    PartLinkT<T,N>::UpdateName()
    {
      UserItem::GetName() = std::string(Part::GetStrType() + '_' + GetLinkT().GetName());
    }


    template<class T, int N>
    inline void
    PartLinkT<T,N>::UpdateIcon()
    {
      GetLinkT().template UpdatePartIcon<N>(Core::UserItem::GetIcon());
    }
    

    template<class T, int N>
    inline void
    PartLinkT<T,N>::DrawShape()
    {
      GetLinkT().GetLinkType().template DrawPart<N>();
    }

    template<class T, int N>
    inline void
    PartLinkT<T,N>::BeginDraw()
    {
      if (N==1)
	GetLinkT().GetLinkType().BeginDraw1();
      else
	GetLinkT().GetLinkType().BeginDraw2();
    }


    template<class T, int N>
    inline Body& 
    PartLinkT<T,N>::GetBody()
    {
      return GetLinkT().template GetBody<N>();
    }
    
    template<class T, int N>
    inline const Body& 
    PartLinkT<T,N>::GetBody() const
    {
      return GetLinkT().template GetBody<N>();
    }

    template<class T, int N>
    inline QTreeWidgetItem& 
    PartLinkT<T,N>::GetParentTreeItem()
    {
      return GetLinkT().template GetBody<N>().GetMainTreeItem();
    }


    template<class T, int N>
    inline const Geom::Frame<_3D>& 
    PartLinkT<T,N>::GetFrame() const
    {
      return frame_;
    }

    template<class T, int N>
    inline Geom::Frame<_3D>& 
    PartLinkT<T,N>::GetFrame()
     {
      return frame_;
    } 

    template<class T, int N>
    inline const Util::Color& 
    PartLinkT<T,N>::GetColor() const
    {
      return GetLinkT().template GetBody<N>().GetColor();
    }
    
    template<class T, int N>
    inline Util::Color& 
    PartLinkT<T,N>::GetColor()
    {
      return GetLinkT().template GetBody<N>().GetColor();
    }


    template<class T, int N>
    inline const Core::AutoRegisteredPtr<LinkT<T>, PartLinkT<T,N> >& 
    PartLinkT<T,N>::GetLinkTPtr() const
    {
      return link_;
    }


    template<class T, int N>
    inline Core::AutoRegisteredPtr<LinkT<T>, PartLinkT<T,N> >& 
    PartLinkT<T,N>::GetLinkTPtr()
    {
      return link_;
    }


    template <class T, int N> 
    inline Core::SetOfBase<Core::UserItem> 
    PartLinkT<T,N>::GetAssociatedSelectedItem()
    {
      Core::SetOfBase<Core::UserItem> set;

      Core::SetOfBase<PartPoint>::it it;
      for (it = points_.Begin() ; it != points_.End(); it++ )
	set.AddItem(**it);

      return set;
    }


    template <class T, int N> 
    inline Geom::Point<_3D>& 
    PartLinkT<T,N>::GetCenter()
    {
      return center_;
    }

    template <class T, int N> 
    inline const Geom::Point<_3D>& 
    PartLinkT<T,N>::GetCenter() const
    {
      return center_;
    }


     template <class T, int N> 
    inline Geom::Quaternion<_3D>&
    PartLinkT<T,N>::GetQuaternion()
    {
      return quaternion_;
    }

    template <class T, int N> 
    inline const Geom::Quaternion<_3D>&
    PartLinkT<T,N>::GetQuaternion() const
    {
      return quaternion_;
    }


  }

}



namespace boost 
{ 
  namespace serialization 
  {
    template<class Archive, class T, int N>
    inline void save_construct_data(Archive & ar, 
				    const OpenMeca::Item::PartLinkT<T,N> * t, 
				    const unsigned int)
    {
      const OpenMeca::Item::LinkT<T>* linkPtr = t->GetLinkTPtr().GetPtr();
      ar << linkPtr;
    }
    
    template<class Archive, class T, int N>
    inline void load_construct_data(Archive & ar, 
				    OpenMeca::Item::PartLinkT<T,N> * t, 
				    const unsigned int)
    {
      OpenMeca::Item::LinkT<T>* linkPtr = 0;
      ar >> linkPtr;
      ::new(t)OpenMeca::Item::PartLinkT<T,N>(*linkPtr);
    }
  }
} // namespace ...



#endif
