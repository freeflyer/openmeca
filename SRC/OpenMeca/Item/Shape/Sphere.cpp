// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QWidget>

#include "OpenMeca/Item/Shape/Sphere.hpp"
#include "OpenMeca/Item/PartUserShapeT.hpp"
#include "OpenMeca/Item/Body.hpp"
#include "OpenMeca/Util/Draw.hpp"
#include "OpenMeca/Gui/Prop/PropTree.hpp"

#include <boost/serialization/export.hpp>
BOOST_CLASS_EXPORT(OpenMeca::Item::PartUserShapeT<OpenMeca::Item::Shape::Sphere>)



namespace OpenMeca
{
  namespace Item
  {
    namespace Shape
    {
    
      const std::string
      Sphere::GetStrType()
      {
	return "Sphere";
      }

      const QString
      Sphere::GetQStrType()
      {
	return QObject::tr("Sphere");
      }
      

      Sphere::Sphere(PartUser& part)
	:ShapeBase(part),
	 radius_(0.)
       
      {
      }
   
      Sphere::~Sphere()
      {
      }
    
      void 
      Sphere::BeginDraw()
      {
      }

      void 
      Sphere::Draw() const
      {
	Util::Draw::Sphere(radius_);
      }

      void 
      Sphere::BuildChSystem(chrono::ChSystem&)
      {
	Body& body = GetPart().GetBody();
	Geom::Frame<_3D>&(Body::*fnptr)() = &Body::GetFrame;
	std::function<const Geom::Frame<_3D>& ()> f = boost::bind(fnptr, boost::ref(body));

	Geom::Point<_3D> p(GetPart().GetPoint(), f);
	chrono::ChVector<> chp = body.ExpressPointInLocalChFrame(p);

	chrono::ChSharedBodyPtr& chbody = body.GetChBodyPtr();
	chbody->GetCollisionModel()->AddSphere(radius_, &chp);
	chbody->SetCollide(true);
      }

    }
  }
}
