// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Item/PartPoint.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Util/Draw.hpp"



#include "OpenMeca/Item/PartPoint_CreateAction_SpecializedT.hpp"

#include <boost/serialization/export.hpp>
BOOST_CLASS_EXPORT(OpenMeca::Item::PartPoint)

namespace OpenMeca
{  
  namespace Core
  {

    template<>
    void
    ItemCommonProperty<OpenMeca::Item::PartPoint>::BuildIconSymbol()
    {
      QIcon icon;
      OpenMeca::Item::PartPoint::DrawIcon(iconSymbol_, Qt::gray);
    }

    template<>
    void 
    UserItemCommonProperty<OpenMeca::Item::PartPoint>::CreateAction_Specialized() 
    {
      // We call PartPoint_CreateAction_SpecializedT because it is needed by other class
      UserItemCommonProperty<OpenMeca::Item::PartPoint>& me = *this;
      OpenMeca::Item::PartPoint_CreateAction_SpecializedT(me);
    }

  }
}


namespace OpenMeca
{  
  namespace Item
  {
    

    void
    PartPoint::Init()
    {
      Core::Singleton< Core::UserItemCommonProperty< PartPoint > >::Get().CreateAction_Specialized();
    }


    void 
    PartPoint::DrawIcon(QIcon& icon, QColor color)
    {
      Util::Icon::DrawIconFromSvgFile(":/Rsc/Img/Part/Point.svg", icon, color);
    }


    PartPoint::PartPoint(Core::UserItem& parent)
      :PartUser(GetStrType(), parent),
       mustBeScaled_(false),
       scaleCoordinate_(boost::bind(&PartUser::GetReferenceFrame, boost::ref(*this)))
    {
    }

  
    PartPoint::PartPoint(const std::string& childStrType, Core::UserItem& parent)
      :PartUser(childStrType, parent),
       mustBeScaled_(false),
       scaleCoordinate_(boost::bind(&PartUser::GetReferenceFrame, boost::ref(*this)))
    {
    }  
    
    PartPoint::~PartPoint()
    {
    }

    void 
    PartPoint::BeginDraw()
    {
      if (mustBeScaled_)
	{
	  const double scale = Part::GetScaleValue();
	  PartUser::GetPoint() = (scaleCoordinate_*scale).GetPoint();
	}
    }

    void 
    PartPoint::DrawShape()
    {
      const double scale = Part::GetScaleValue();
      Util::Draw::Sphere(.02f*scale);
    }

    void 
    PartPoint::UpdateIcon()
    {
      PartPoint::DrawIcon(GetIcon(), GetColor().GetQColor());
    }
    
    void
    PartPoint::SetScaleCoordinate(double x, double y, double z)
    {
      mustBeScaled_ = true;
      scaleCoordinate_[0] = x;
      scaleCoordinate_[1] = y;
      scaleCoordinate_[2] = z;
    }

    void
    PartPoint::SetAbsoluteCoordinate(double x, double y, double z)
    {
      mustBeScaled_ = false;
      PartUser::GetPoint()[0] = x;
      PartUser::GetPoint()[1] = y;
      PartUser::GetPoint()[2] = z;
    }

  }
}




