// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef OpenMeca_Item_PartPoint_CreateAction_SpecializedT_hpp
#define OpenMeca_Item_PartPoint_CreateAction_SpecializedT_hpp


#include "OpenMeca/Core/UserItemCommonProperty.hpp"


#include "OpenMeca/Item/PartUserPipe.hpp"
#include "OpenMeca/Item/PartUserJunction.hpp"

#include "OpenMeca/Item/PartUserShapeT.hpp"
#include "OpenMeca/Item/Shape/Sphere.hpp"
#include "OpenMeca/Item/Shape/Cylinder.hpp"
#include "OpenMeca/Item/Shape/Box.hpp"
#include "OpenMeca/Item/Shape/Ground.hpp"

#include "OpenMeca/Item/How/GetLinVel.hpp"
#include "OpenMeca/Item/How/GetLinAcc.hpp"
#include "OpenMeca/Item/How/GetAngPos.hpp"
#include "OpenMeca/Item/How/GetAngVel.hpp"
#include "OpenMeca/Item/How/GetAngAcc.hpp"
#include "OpenMeca/Item/How/GetLinPos.hpp"
#include "OpenMeca/Item/How/SetForce.hpp"
#include "OpenMeca/Item/How/SetTorque.hpp"

namespace OpenMeca
{  
  namespace Item
  {


    template<class T>
    inline void
    PartPoint_CreateAction_SpecializedT(OpenMeca::Core::UserItemCommonProperty<T>& t)
    {
      const QIcon emptyIcon;

      const std::string geomTitle = "Geometry";
      t.AddPopUpSubMenu(geomTitle, emptyIcon);

      t.template CreateAction_NewWithSelection<OpenMeca::Item::PartUserPipe>(geomTitle);
      t.template CreateAction_NewWithSelection<OpenMeca::Item::PartUserJunction>(geomTitle);

      typedef OpenMeca::Item::PartUserShapeT<OpenMeca::Item::Shape::Sphere>    PartSphere;
      typedef OpenMeca::Item::PartUserShapeT<OpenMeca::Item::Shape::Box>       PartBox;
      typedef OpenMeca::Item::PartUserShapeT<OpenMeca::Item::Shape::Cylinder>  PartCylinder;
      typedef OpenMeca::Item::PartUserShapeT<OpenMeca::Item::Shape::Ground>  PartGround;
      Core::Singleton< Core::UserItemCommonProperty<PartSphere> >  ::Instanciate();
      Core::Singleton< Core::UserItemCommonProperty<PartBox> >     ::Instanciate();
      Core::Singleton< Core::UserItemCommonProperty<PartCylinder> >::Instanciate();
      Core::Singleton< Core::UserItemCommonProperty<PartGround> >  ::Instanciate();
      t.template CreateAction_NewWithSelection<PartSphere>(geomTitle);
      t.template CreateAction_NewWithSelection<PartBox>(geomTitle);
      t.template CreateAction_NewWithSelection<PartCylinder>(geomTitle);
      t.template CreateAction_NewWithSelection<PartGround>(geomTitle);

      const std::string loadTitle = "Loading";
      t.AddPopUpSubMenu(loadTitle, emptyIcon);
      typedef OpenMeca::Item::How::SetForce::MyLoad LoadForce;
      typedef OpenMeca::Item::How::SetTorque::MyLoad LoadTorque;
      Core::Singleton<Core::UserItemCommonProperty<LoadForce> >::Instanciate();
      Core::Singleton<Core::UserItemCommonProperty<LoadTorque> >::Instanciate();
      t.template CreateAction_NewWithSelection<LoadForce>(loadTitle);
      t.template CreateAction_NewWithSelection<LoadTorque>(loadTitle);

      const std::string sensorTitle = "Sensor";
      t.AddPopUpSubMenu(sensorTitle, emptyIcon);
      typedef OpenMeca::Item::How::GetLinPos::MySensor SensorLinPos;
      typedef OpenMeca::Item::How::GetLinVel::MySensor SensorLinVel;
      typedef OpenMeca::Item::How::GetLinAcc::MySensor SensorLinAcc;
      typedef OpenMeca::Item::How::GetAngPos::MySensor SensorAngPos;
      typedef OpenMeca::Item::How::GetAngVel::MySensor SensorAngVel;
      typedef OpenMeca::Item::How::GetAngAcc::MySensor SensorAngAcc;
      Core::Singleton<Core::UserItemCommonProperty<SensorLinPos> >::Instanciate();
      Core::Singleton<Core::UserItemCommonProperty<SensorLinVel> >::Instanciate();
      Core::Singleton<Core::UserItemCommonProperty<SensorLinAcc> >::Instanciate();
      Core::Singleton<Core::UserItemCommonProperty<SensorAngPos> >::Instanciate();
      Core::Singleton<Core::UserItemCommonProperty<SensorAngVel> >::Instanciate();
      Core::Singleton<Core::UserItemCommonProperty<SensorAngAcc> >::Instanciate();
      t.template CreateAction_NewWithSelection<SensorLinPos>(sensorTitle);
      t.template CreateAction_NewWithSelection<SensorLinVel>(sensorTitle);
      t.template CreateAction_NewWithSelection<SensorLinAcc>(sensorTitle);
      t.template CreateAction_NewWithSelection<SensorAngPos>(sensorTitle);
      t.template CreateAction_NewWithSelection<SensorAngVel>(sensorTitle);
      t.template CreateAction_NewWithSelection<SensorAngAcc>(sensorTitle);
    }

  }
}


#endif
