// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef OpenMeca_Item_Link_CreateAction_SpecializedT_hpp
#define OpenMeca_Item_Link_CreateAction_SpecializedT_hpp


#include "OpenMeca/Core/UserItemCommonProperty.hpp"
#include "OpenMeca/Item/How/GetReacForce.hpp"
#include "OpenMeca/Item/How/GetReacTorque.hpp"


namespace OpenMeca
{  
  namespace Item
  {


    template<class T>
    inline void
    Link_CreateAction_SpecializedT(OpenMeca::Core::UserItemCommonProperty<T>& t)
    {
      t.AddPopUpSeparator();
      const QIcon emptyIcon;
      const std::string sensorTitle = "Sensor";

      typedef OpenMeca::Item::How::GetReacForce::MySensor SensorReacForce;
      typedef OpenMeca::Item::How::GetReacTorque::MySensor SensorReacTorque;
      Core::Singleton<Core::UserItemCommonProperty<SensorReacForce> >::Instanciate();
      Core::Singleton<Core::UserItemCommonProperty<SensorReacTorque> >::Instanciate();
      t.template CreateAction_NewWithSelection<SensorReacForce>();
      t.template CreateAction_NewWithSelection<SensorReacTorque>();

    
    }

  }
}


#endif
