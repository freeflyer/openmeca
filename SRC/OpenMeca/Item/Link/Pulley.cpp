// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Item/Link/Pulley.hpp"
#include "OpenMeca/Item/LinkT.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Core/Drawable.hpp"
#include "OpenMeca/Core/System.hpp"

#include "OpenMeca/Core/Drawable.hpp" 
#include "OpenMeca/Item/PartPoint.hpp" 
#include "OpenMeca/Util/Draw.hpp"
#include "OpenMeca/Item/LinkT.hpp" 




namespace OpenMeca
{  
  namespace Item
  {
    const std::string 
    Pulley::GetStrType()
    {
      return "Pulley";
    }

    const QString 
    Pulley::GetQStrType()
    {
      return QObject::tr("Pulley");
    }

    Pulley::Pulley(Link& link)
      :LinkTypeBase(link),
       radius1_(0.),
       radius2_(0.),
       interAxisLength_(0.),
       modulus_(0.01)
    {
    }

    Pulley::~Pulley()
    {
    }


    template<> 
    double 
    Pulley::ComputePinionBeltAngle<1>()
    {
      return 2*acos((radius1_ - radius2_) /  interAxisLength_);
    }

    template<> 
    double 
    Pulley::ComputePinionBeltAngle<2>()
    {
      return M_PI - ComputePinionBeltAngle<1>();
    }


    template<> 
    double 
    Pulley::ComputeRotationAngle<1>()
    {
      const Geom::Point<_3D> O2(GetLink().GetPart2().GetCenter(), 
				GetLink().GetPart1().GetFrameFct());
      const Geom::Vector<_3D> dir = O2.GetPositionVector().Unit();
      const Geom::Vector<_3D>& x = GetLink().GetPart1().GetFrame().GetYAxis();

      Geom::Quaternion<_3D> Q(GetLink().GetPart1().GetFrameFct());
      Q.SetVecFromTo(x, dir);
      float angle = Q.GetAngle();
      if (Q.GetAxis()[2] < 0)
	angle = 2*M_PI - angle;
      return  angle;
    }

    template<> 
    double 
    Pulley::ComputeRotationAngle<2>()
    {
      const Geom::Point<_3D> O1(GetLink().GetPart1().GetCenter(), 
				GetLink().GetPart2().GetFrameFct());
      const Geom::Vector<_3D> dir = O1.GetPositionVector().Unit();
      const Geom::Vector<_3D>& x = GetLink().GetPart2().GetFrame().GetXAxis();
      Geom::Quaternion<_3D> Q(GetLink().GetPart2().GetFrameFct());
      Q.SetVecFromTo(x, dir);
      float angle = Q.GetAngle();
      if (Q.GetAxis()[2] < 0)
	angle = 2*M_PI - angle;
      return  angle;
    }

    double 
    Pulley::ComputeRackLength()
    {
      const float e  = interAxisLength_;
      const float d1 = radius1_*2.; 
      const float d2 = radius2_*2.; 
      return e*sqrt(1 - pow((d2-d1)/(2.*e), 2));
    }

    template<> 
    void 
    Pulley::DrawPart<1>()
    {
      const double scale = Part::GetScaleValue();
      const float beltThick = 0.01f*scale;

      const float l  = 0.1f*scale;
      const float L = ComputeRackLength();
      const float e  = beltThick;
      const float Ri = radius1_;
      const float Re = Ri + e;
      const float th = (2.*M_PI - ComputePinionBeltAngle<1>())/2.;
      
      
      

      glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
      Util::Draw::Cylinder(Ri, l);
      glLineWidth(1.);
      Util::Draw::CylinderStripe(Ri, l);

      Util::Color::Black.ApplyGLColor();
      glLineWidth(2.5);
      glBegin(GL_LINES);
      glVertex3f(-l/2., Ri, 0.0f);
      glVertex3f(-l/2.,-Ri, 0.0f);
      glVertex3f( l/2., Ri, 0.0f);
      glVertex3f( l/2.,-Ri, 0.0f);
      glVertex3f(-l/2., 0.0f, Ri);
      glVertex3f(-l/2., 0.0f,-Ri);
      glVertex3f( l/2., 0.0f, Ri);
      glVertex3f( l/2., 0.0f,-Ri);
      glEnd();

      const double rot = ComputeRotationAngle<1>()*180./M_PI;
      glRotatef(rot, 1.0f, 0.0f, 0.0f);
      glRotatef(180.f, 1.0f, 0.0f, 0.0f);
      Util::Draw::Cylinder(Ri, Re, l, -th, th);

      glRotatef(th*180./M_PI, 0.0f, 0.0f, 1.0f);
      glTranslatef(-L/2., Ri + e/2, 0.0f);
      Util::Draw::Box(L, e, l);
      glTranslatef(L/2., -Ri - e/2, 0.0f);
      
      glRotatef(-2.*th*180./M_PI, 0.0f, 0.0f, 1.0f);
      glTranslatef(L/2., Ri + e/2, 0.0f);
      Util::Draw::Box(L, e, l);

      
    }


    template<> 
    void 
    Pulley::DrawPart<2>()
    {
      const double scale = Part::GetScaleValue();
      const float beltThick = 0.01f*scale;

      const float l  = 0.1f*scale;
      const float e  = beltThick;
      const float Ri = radius2_;
      const float Re = Ri + e;
      const float th = ComputePinionBeltAngle<1>()/2.;
      glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
      Util::Draw::Cylinder(Ri, l);
      
      glLineWidth(1.);
      Util::Draw::CylinderStripe(Ri, l);

      Util::Color::Black.ApplyGLColor();
      glLineWidth(2.5);
      glBegin(GL_LINES);
      glVertex3f(-l/2., Ri, 0.0f);
      glVertex3f(-l/2.,-Ri, 0.0f);
      glVertex3f( l/2., Ri, 0.0f);
      glVertex3f( l/2.,-Ri, 0.0f);
      glVertex3f(-l/2., 0.0f, Ri);
      glVertex3f(-l/2., 0.0f,-Ri);
      glVertex3f( l/2., 0.0f, Ri);
      glVertex3f( l/2., 0.0f,-Ri);
      glEnd();

      const double rot = ComputeRotationAngle<2>()*180./M_PI;
      glRotatef(rot+90., 1.0f, 0.0f, 0.0f);
      Util::Draw::Cylinder(Ri, Re, l, -th, th);
    }


    void 
    Pulley::UpdatePart()
    {
      GetLink().GetPart2().GetCenter()[1] = interAxisLength_;
    }


    


  }
}




