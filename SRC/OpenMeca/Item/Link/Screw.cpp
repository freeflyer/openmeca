// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Item/Link/Screw.hpp"
#include "OpenMeca/Item/LinkT.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Core/Drawable.hpp"
#include "OpenMeca/Core/System.hpp"

#include "OpenMeca/Core/Drawable.hpp" 
#include "OpenMeca/Item/PartPoint.hpp" 
#include "OpenMeca/Util/Draw.hpp"
#include "OpenMeca/Item/LinkT.hpp" 




namespace OpenMeca
{  
  namespace Item
  {
    const std::string 
    Screw::GetStrType()
    {
      return "Screw";
    }

    const QString
    Screw::GetQStrType()
    {
      return QObject::tr("Screw");
    }

    Screw::Screw(Link& link)
      :LinkTypeBase(link),
       tau_(0.)
    {
    }
    
    Screw::~Screw()
    {
    }

    template<> 
    void 
    Screw::DrawPart<1>()
    {
      const double scale = Part::GetScaleValue();
      const float r = 0.1f*scale;
      const float l = 0.6f*scale;

      glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
      Util::Draw::Cylinder(r,l);

    }

    

    template<>
    void 
    Screw::BuildPoints<1>(Core::SetOf<PartPoint>& set, Core::DrawableUserItem& item)
    {
      OMC_ASSERT_MSG(set.GetTotItemNumber()==0, "The number of point is not null");
      PartPoint* p1 = new PartPoint(item);
      p1->GetName() = "middle";
      p1->SetScaleCoordinate(0., 0., 0.);
      set.AddItem(*p1);

    }


    template<> 
    void 
    Screw::DrawPart<2>()
    {
      const double tau = tau_;
      const double scale = Part::GetScaleValue();
      const GLfloat r = 0.01f*scale;
      const GLfloat l = 1.0f*scale;

      glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
      Util::Draw::Cylinder(r, l);

      // draw helical part
      if (tau > 0.)
	{
	  glBegin(GL_TRIANGLE_STRIP);
	  bool loop = true;
	  float x = -l/2.;
	  float a = 0.;
	  do 
	    {
	      glVertex3f(x, r*cos(a)   , r*sin(a));
	      glVertex3f(x, r*2.*cos(a), r*2.*sin(a));
	      a += M_PI/100.; 
	      x = (-l/2.) + (a * tau);
	      if (x > l/2)
		loop = false;
	    }
	  while(loop == true);
	  glEnd();
	}

    }


    template<>
    void 
    Screw::BuildPoints<2>(Core::SetOf<PartPoint>& set, Core::DrawableUserItem& item)
    {
      OMC_ASSERT_MSG(set.GetTotItemNumber()==0, "The number of point is not null");
      PartPoint* p1 = new PartPoint(item);
      PartPoint* p2 = new PartPoint(item);
      PartPoint* p3 = new PartPoint(item);
      p1->GetName() = "left";
      p1->SetScaleCoordinate(0., 0., -0.5);
      p2->GetName() = "middle";
      p2->SetScaleCoordinate(0., 0., 0.);
      p3->GetName() = "right";
      p3->SetScaleCoordinate(0., 0., 0.5);
      set.AddItem(*p1);
      set.AddItem(*p2);
      set.AddItem(*p3);
    }


  }
}




