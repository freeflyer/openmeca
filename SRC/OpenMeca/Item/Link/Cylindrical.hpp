// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_Link_Cylindrical_hpp
#define OpenMeca_Item_Link_Cylindrical_hpp

#include <string>
#include <QIcon>
#include <QColor>

#include "OpenMeca/Item/Link/LinkTypeBase.hpp"
#include "OpenMeca/Core/ItemCommonProperty.hpp"
#include "OpenMeca/Item/LinkT.hpp"
#include "OpenMeca/Item/Link_CreateAction_SpecializedT.hpp"


namespace OpenMeca
{  
  namespace Item
  {

    class Cylindrical : public LinkTypeBase
    {
    public:
      static const std::string GetStrType();
      static const QString GetQStrType();

      typedef chrono::ChLinkLockCylindrical ChLink; 
      typedef Gui::DialogLinkT< LinkT<Cylindrical> > Dialog;
      

    public:
      Cylindrical(Link&);
      ~Cylindrical();

      template <int N> void DrawPart();
      template <int N> void BuildPoints(Core::SetOf<PartPoint>&, Core::DrawableUserItem&);

    private:
      friend class boost::serialization::access;
      template<class Archive> void serialize(Archive& ar, const unsigned int version);

    };


    template<class Archive>
    inline void
    Cylindrical::serialize(Archive& ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(LinkTypeBase);
    }


   
  }

}



namespace OpenMeca
{  
  namespace Core
  {
    
    template<> 
    inline void
    ItemCommonProperty< OpenMeca::Item::LinkT<OpenMeca::Item::Cylindrical> >::BuildIconSymbol()
    {
      OpenMeca::Item::LinkT<OpenMeca::Item::Cylindrical>::BuildIconSymbol(iconSymbol_);
    }

    template<>
    inline void
    UserItemCommonProperty<OpenMeca::Item::LinkT<OpenMeca::Item::Cylindrical> >::CreateAction_Specialized() 
    {
      OpenMeca::Item::Link_CreateAction_SpecializedT(*this);
    }



  }
}


#endif
