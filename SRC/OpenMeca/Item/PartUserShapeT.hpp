// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_PartUserGeometryT_hpp
#define OpenMeca_Item_PartUserGeometryT_hpp


#include "OpenMeca/Item/PartUserShape.hpp"
#include "OpenMeca/Core/AutoRegisteredPtr.hpp"
#include "OpenMeca/Core/AutoRegister.hpp"
#include "OpenMeca/Core/UserItemCommonProperty.hpp"
#include "OpenMeca/Core/ItemCommonProperty.hpp"
#include "OpenMeca/Gui/Dialog/DialogPartUserShapeT.hpp"
#include "OpenMeca/Item/PartPoint.hpp"
#include "OpenMeca/Util/Icon.hpp"

namespace OpenMeca
{  

  namespace Item
  {

    template <class T>
    class PartUserShapeT: public PartUserShape, 
			  public Core::AutoRegister<PartUserShapeT<T> > 
    {
    
    public:
      static const std::string GetStrType() {return T::GetStrType();};
      static const QString GetQStrType() {return T::GetQStrType();};


      static void Init();
      static void DrawIcon(QIcon&, QColor);
      typedef Gui::DialogPartUserShapeT<T> Dialog;


    public:
      PartUserShapeT(PartPoint& point);
      virtual ~PartUserShapeT();
      void DrawShape();
      void UpdateIcon();
      void BeginDraw();

      T& GetShape();
      const T& GetShape() const;

      bool& GetCollisionState();
      const bool& GetCollisionState() const;


      void BuildChSystem(chrono::ChSystem&);

    private:
      PartUserShapeT(); //Not allowed, just for serialization
      PartUserShapeT(const PartUserShapeT<T>&);             //Not Allowed
      PartUserShapeT& operator=(const PartUserShapeT<T>&);  //Not Allowed


      friend class boost::serialization::access;
      template<class Archive> void save(Archive& ar, const unsigned int) const;
      template<class Archive> void load(Archive& ar, const unsigned int);
      BOOST_SERIALIZATION_SPLIT_MEMBER()

    private:
      T shape_;
      bool collisionEnabled_;
    };

    template <class T>
    template<class Archive>
    inline void 
    PartUserShapeT<T>::save(Archive& ar, const unsigned int) const
    {
      ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(PartUserShape);
      ar << BOOST_SERIALIZATION_NVP(shape_);
      ar << BOOST_SERIALIZATION_NVP(collisionEnabled_);
    }
    
    template <class T>
    template<class Archive>
    inline void 
    PartUserShapeT<T>::load(Archive& ar, const unsigned int)
    {
      ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(PartUserShape);
      ar >> BOOST_SERIALIZATION_NVP(shape_);
      ar >> BOOST_SERIALIZATION_NVP(collisionEnabled_);
    }



    template <class T>
    inline void
    PartUserShapeT<T>::Init()
    {
      Core::UserItemCommonProperty< PartUserShapeT<T> > & prop = 
	Core::Singleton< Core::UserItemCommonProperty< PartUserShapeT<T> > >::Get();

      prop.CreateAction_Edit();
      prop.CreateAction_Delete();

      // don't forget to instanciate UserRootItemCommonProperty<Body> before calling
      // create action
      Core::Singleton< Core::UserItemCommonProperty<PartPoint> >::Instanciate();
      prop.template CreateAction_NewWithAutomaticSelection<PartPoint>();
    }
    
    template <class T>
    inline void 
    PartUserShapeT<T>::DrawIcon(QIcon& icon, QColor color)
    {
      // Read svg icon file
      QString svgFileName = ":/Rsc/Img/Shape/" + QString(T::GetStrType().c_str()) + ".svg";
      Util::Icon::DrawIconFromSvgFile(svgFileName, icon, color);
    }
    
    template <class T>
    inline
    PartUserShapeT<T>::PartUserShapeT(PartPoint& parent)
      :PartUserShape(GetStrType(), parent),
       shape_(*this),
       collisionEnabled_(false)
    {
    }

    template <class T>
    inline
    PartUserShapeT<T>::~PartUserShapeT()
    {
    }

    template <class T>
    inline void 
    PartUserShapeT<T>::BeginDraw()
    {
      Geom::Point<_3D>& p = PartUser::GetPoint();
      p = Geom::Point<_3D>(GetStartPoint().GetPoint(),
			   boost::bind(&PartUserShapeT<T>::GetReferenceFrame, boost::ref(*this)));
      shape_.BeginDraw();
    }

    template <class T>
    inline void 
    PartUserShapeT<T>::DrawShape()
    {
      shape_.Draw();
    }

    template <class T>
    inline void 
    PartUserShapeT<T>::UpdateIcon()
    {
      DrawIcon(GetIcon(), GetColor().GetQColor());
    }

    template <class T>
    inline T& 
    PartUserShapeT<T>::GetShape()
    {
      return shape_;
    }

    template <class T>
    inline const T& 
    PartUserShapeT<T>::GetShape() const
    {
      return shape_;
    }

    template <class T>
    inline void 
    PartUserShapeT<T>::BuildChSystem(chrono::ChSystem& chsystem)
    {
      if (GetCollisionState())
	shape_.BuildChSystem(chsystem);
    }


    template <class T>
    inline bool& 
    PartUserShapeT<T>::GetCollisionState()
    {
      return collisionEnabled_;
    }
    
    template <class T>
    inline const bool& 
    PartUserShapeT<T>::GetCollisionState() const
    {
      return collisionEnabled_;
    }   

  }

}




namespace boost 
{ 
  namespace serialization 
  {
    

    template<class Archive, class T>
    inline void save_construct_data(Archive & ar, 
				    const OpenMeca::Item::PartUserShapeT<T> * t, 
				    const unsigned int)
    {
      const OpenMeca::Item::PartPoint* parent = &t->GetStartPoint();
      ar << parent;
    }
    

    template<class Archive, class T>
    inline void load_construct_data(Archive & ar, 
				    OpenMeca::Item::PartUserShapeT<T> * t, 
				    const unsigned int)
    {
      OpenMeca::Item::PartPoint* parent = 0;
      ar >> parent;
      ::new(t)OpenMeca::Item::PartUserShapeT<T>(*parent);
    }
  }
} // namespace ...





#endif
