// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


// This source file was inspired from the "libItem" from 
// the GranOO workbench : http://www.granoo.org and


#include "OpenMeca/Item/Physical.hpp"
#include "OpenMeca/Setting/Simulation.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Gui/RootTreeItemT.hpp"
#include "OpenMeca/Physic/Type.hpp"

#include <boost/serialization/export.hpp>
BOOST_CLASS_EXPORT(OpenMeca::Item::Physical)


namespace OpenMeca
{
  namespace Item
  {    

    const std::string 
    Physical::GetStrType()
    {
      return std::string("Physical");
    }

    void
    Physical::DumpAllDataToFile(const std::string& fileName)
    {
      std::ofstream dataFile;
      dataFile.open (fileName.c_str());
      OMC_ASSERT_MSG(dataFile.is_open(), "can't open data file");
      dataFile.precision(10);
      dataFile << std::scientific;

      const std::vector<double>& time = 
	Core::SystemSettingT<Setting::Simulation>::Get().GetTimeArray();
      const unsigned int totIter = time.size();

      Core::SetOfBase<Physical>& phys = 
	Core::System::Get().GetSetOf<Physical>();
      
      dataFile << "Time (s)";
      for (unsigned int i = 0; i < phys.GetTotItemNumber(); ++i)
	{
	  dataFile << '\t';
	  phys(i).WriteHeaderDataFile(dataFile);
	}
      dataFile << '\n';
      for (unsigned int iter = 0; iter < totIter; ++iter)
	{
	  dataFile << time[iter];
	  for (unsigned int i = 0; i < phys.GetTotItemNumber(); ++i)
	    {
	      dataFile << '\t';
	      phys(i).WriteDataFile(dataFile, iter);
	    }
	  dataFile << '\n';
	}
      dataFile.close();
    }


       
    Physical::Physical(const std::string strType, QTreeWidgetItem& parentTreeItem)
      :Core::DrawableUserItem(strType, parentTreeItem, false)
    {
    }
    
    Physical::~Physical()
    {
      
    }


    void 
    Physical::FillDataTree(QTreeWidgetItem* item)
    {
      GetQuantity().GetDataType().FillDataTree(item);
    }

    void 
    Physical::UpdateDataTree()
    {
      GetQuantity().GetDataType().UpdateDataTree();
    }

    const Util::Unit& 
    Physical::GetUnit() const
    {
      return GetQuantity().GetUnit();
    }

    void 
    Physical::WriteHeaderDataFile(std::ofstream& ofs)
    {
      return GetQuantity().GetDataType().WriteHeaderDataFile(ofs);
    }

    void 
    Physical::WriteDataFile(std::ofstream& ofs, unsigned int i)
    {
      return GetQuantity().GetDataType().WriteDataFile(ofs, i);
    }

    const Util::Color& 
    Physical::GetColor() const
    {
      return GetQuantity().GetColor();
    }

    Util::Color& 
    Physical::GetColor()
    {
      return GetQuantity().GetColor();
    }

    const Body& 
    Physical::GetBody() const
    {
      return GetParent().GetBody();
    }

    Body& 
    Physical::GetBody()
    {
      return GetParent().GetBody();
    }

  } 
}
