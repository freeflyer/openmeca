// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Item/PartLinkT.hpp"
#include "OpenMeca/Item/LinkT.hpp"
#include "OpenMeca/Item/Link/All.hpp"



#include <boost/serialization/export.hpp>
typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::Revolute,1> PartLinkTRevolute1;
typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::Revolute,2> PartLinkTRevolute2;
BOOST_CLASS_EXPORT(PartLinkTRevolute1)
BOOST_CLASS_EXPORT(PartLinkTRevolute2)

typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::Motor,1> PartLinkTMotor1;
typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::Motor,2> PartLinkTMotor2;
BOOST_CLASS_EXPORT(PartLinkTMotor1)
BOOST_CLASS_EXPORT(PartLinkTMotor2)

typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::Slider,1> PartLinkTSlider1;
typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::Slider,2> PartLinkTSlider2;
BOOST_CLASS_EXPORT(PartLinkTSlider1)
BOOST_CLASS_EXPORT(PartLinkTSlider2)

typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::Cylindrical,1> PartLinkTCylindrical1;
typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::Cylindrical,2> PartLinkTCylindrical2;
BOOST_CLASS_EXPORT(PartLinkTCylindrical1)
BOOST_CLASS_EXPORT(PartLinkTCylindrical2)

typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::Planar,1> PartLinkTPlanar1;
typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::Planar,2> PartLinkTPlanar2;
BOOST_CLASS_EXPORT(PartLinkTPlanar1)
BOOST_CLASS_EXPORT(PartLinkTPlanar2)

typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::PointLine,1> PartLinkTPointLine1;
typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::PointLine,2> PartLinkTPointLine2;
BOOST_CLASS_EXPORT(PartLinkTPointLine1)
BOOST_CLASS_EXPORT(PartLinkTPointLine2)

typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::PointPlane,1> PartLinkTPointPlane1;
typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::PointPlane,2> PartLinkTPointPlane2;
BOOST_CLASS_EXPORT(PartLinkTPointPlane1)
BOOST_CLASS_EXPORT(PartLinkTPointPlane2)

typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::Spherical,1> PartLinkTSpherical1;
typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::Spherical,2> PartLinkTSpherical2;
BOOST_CLASS_EXPORT(PartLinkTSpherical1)
BOOST_CLASS_EXPORT(PartLinkTSpherical2)

typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::Screw,1> PartLinkTScrew1;
typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::Screw,2> PartLinkTScrew2;
BOOST_CLASS_EXPORT(PartLinkTScrew1)
BOOST_CLASS_EXPORT(PartLinkTScrew2)

typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::Gear,1> PartLinkTGear1;
typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::Gear,2> PartLinkTGear2;
BOOST_CLASS_EXPORT(PartLinkTGear1)
BOOST_CLASS_EXPORT(PartLinkTGear2)

typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::Pulley,1> PartLinkTPulley1;
typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::Pulley,2> PartLinkTPulley2;
BOOST_CLASS_EXPORT(PartLinkTPulley1)
BOOST_CLASS_EXPORT(PartLinkTPulley2)

typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::RackPinion,1> PartLinkTRackPinion1;
typedef OpenMeca::Item::PartLinkT<OpenMeca::Item::RackPinion,2> PartLinkTRackPinion2;
BOOST_CLASS_EXPORT(PartLinkTRackPinion1)
BOOST_CLASS_EXPORT(PartLinkTRackPinion2)


namespace OpenMeca
{  
  namespace Item
  {



  }
}




