// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_Variable_hpp
#define OpenMeca_Item_Variable_hpp

#include "OpenMeca/Core/UserRootItem.hpp"
#include "OpenMeca/Core/AutoRegister.hpp"
#include "OpenMeca/Gui/Dialog/DialogVariable.hpp"
#include "OpenMeca/Util/Icon.hpp"
#include "OpenMeca/Core/ItemCommonProperty.hpp"
#include "OpenMeca/Util/Var.hpp"



namespace OpenMeca
{  
  namespace Item
  {


    // The physical rigid body
    class Variable: public Core::UserRootItem, public Core::AutoRegister<Variable>,
		    public Util::Var
    {
    
    public:
      static const std::string GetStrType() {return "Variable";}; 
      static const QString GetQStrType() {return QObject::tr("Variable");}; 
      static void Init();

      friend class  Gui::DialogVariable;
      typedef Gui::DialogVariable Dialog;

    public:
      Variable();
      virtual ~Variable();

      void UpdateIcon();


    private:
      Variable(const Variable&);             //Not Allowed
      Variable& operator=(const Variable&);  //Not Allowed
      
      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive & ar, const unsigned int);

    };


    template<class Archive>
    inline void
    Variable::serialize(Archive & ar, const unsigned int)
      {
	ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Core::UserRootItem);
	ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Util::Var);
      }


  }

}


namespace OpenMeca
{  
  namespace Core
  {

    template<>
    inline void
    ItemCommonProperty<OpenMeca::Item::Variable>::BuildIconSymbol()
    {
      iconSymbol_ = Util::Icon::DrawIconFromSvgFile(":/Rsc/Img/Equation.svg");
    }

  }
}




#endif
