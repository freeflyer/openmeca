## This file is part of OpenMeca, an easy software to do mechanical simulation.
##
## Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
##
## Copyright (C) 2012 Damien ANDRE
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

include(./Action/Action.pro)

HEADERS += Core/AutoRegisteredPtr.hpp \
           Core/Macro.hpp \
           Core/ConfigDirectory.hpp \
           Core/Condition.hpp \
           Core/GlobalSetting.hpp \
           Core/ItemCommonProperty.hpp \
           Core/SelectionManager.hpp \
           Core/Singleton.hpp \
           Core/SystemSetting.hpp \
           Core/UserItem.hpp \
           Core/ConfigFile.hpp \
           Core/XmlConfigFile.hpp \
           Core/AutoRegister.hpp \
           Core/Drawable.hpp \
           Core/DrawableUserItem.hpp \
           Core/GlobalSettingT.hpp \
           Core/Item.hpp \
           Core/SetOfBase.hpp \
           Core/System.hpp \
           Core/Software.hpp \
           Core/SystemSettingT.hpp \
           Core/UserRootItemCommonProperty.hpp \
           Core/CommonProperty.hpp \
           Core/GlobalSettingCommonProperty.hpp \
           Core/HistoricManager.hpp \
           Core/None.hpp \
           Core/SetOf.hpp \
           Core/SystemSettingCommonProperty.hpp \
           Core/UserItemCommonProperty.hpp \
           Core/UserRootItem.hpp

SOURCES += Core/Macro.cpp \
           Core/CommonProperty.cpp \
           Core/ConfigDirectory.cpp \
           Core/Drawable.cpp \
           Core/DrawableUserItem.cpp \
           Core/GlobalSetting.cpp \
           Core/HistoricManager.cpp \
           Core/Item.cpp \
           Core/SelectionManager.cpp \
           Core/SetOfBase.cpp \
           Core/SetOf.cpp \
           Core/System.cpp \
           Core/Software.cpp \
           Core/SystemSetting.cpp \
           Core/UserItem.cpp \
           Core/UserRootItem.cpp \
           Core/ConfigFile.cpp \
           Core/XmlConfigFile.cpp
