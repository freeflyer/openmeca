// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_UserRootItem_hpp
#define OpenMeca_Core_UserRootItem_hpp


#include "OpenMeca/Core/UserItem.hpp"


namespace OpenMeca
{
  namespace Core
  {
    // The user root item base class.
    // An user root item is an user item that is root in the openmeca hierarchy.
    // An user root item have not any parent. 
    // For example, this is a body or a link.
    class UserRootItem : public UserItem, public AutoRegister<UserRootItem>
    {
    public: 
      static const std::string GetStrType(); 

      UserRootItem(const std::string strType, QTreeWidgetItem& parent);
      virtual ~UserRootItem();

    private :
      UserRootItem();                                //Not Allowed
      UserRootItem(const UserRootItem&);             //Not Allowed
      UserRootItem& operator=(const UserRootItem&);  //Not Allowed
      
      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive & ar, const unsigned int)
      {
	ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(UserItem);
      }


    }; 
    

  }
}


#endif
