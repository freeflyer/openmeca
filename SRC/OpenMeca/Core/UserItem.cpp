// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.



#include <iostream>
#include <QMessageBox>

#include "OpenMeca/Core/UserItem.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Gui/MainPlotWindow.hpp"

namespace OpenMeca
{
  namespace Core
  {


    UserItem::UserItem(const std::string strType,  QTreeWidgetItem& parent)
      :Item(strType, &parent),
       name_(strType),
       dependentItems_(),
       parentsItems_()
    {
      name_ = System::Get().GetAutomaticName(strType);
    }


    UserItem::~UserItem()
    {
      dependentItems_.DeleteAllItem();
      Gui::MainPlotWindow::Get().Clear();
    }



    void 
    UserItem::AddChildItem(UserItem& item)
    {
      // std::cout << "-- begin --" << std::endl;
      // std::cout << "  debug, UserItem::AddChildItem, id=" << GetName() << std::endl;
      // std::cout << "  child item is : " << item.GetName() << std::endl;;
      
      // std::cout << "  dependant item contain : " << std::endl;
      // SetOf<UserItem>& set = dependentItems_;
      // SetOf<UserItem>::it it;
      // for (it=set.Begin() ; it != set.End(); it++ )
      // 	{
      // 	  std::cout << "    - " << (*it)->GetName() << std::endl;
      // 	}
      dependentItems_.AddItem(item);
      // std::cout << "  now, dependant item contain : " << std::endl;
      // for (it=set.Begin() ; it != set.End(); it++ )
      // 	{
      // 	  std::cout << "    - " << (*it)->GetName() << std::endl;
      // 	}

      // std::cout << "-- end --" << std::endl;
    }

    void 
    UserItem::EraseChildItem(UserItem& item)
    {
      // std::cout << "-- begin --" << std::endl;
      // std::cout << "  debug, UserItem::EraseChildItem, id=" << GetName() << std::endl;
      // std::cout << "  child item is : " << item.GetName();
      // std::cout << "  dependant item contain : " << std::endl;
      // SetOf<UserItem>& set = dependentItems_;
      // SetOf<UserItem>::it it;
      // for (it=set.Begin() ; it != set.End(); it++ )
      // 	std::cout << "    - " << (*it)->GetName() << std::endl;

      dependentItems_.RemoveItem(item);      

      // std::cout << "  now, dependant item contain : " << std::endl;
      // for (it=set.Begin() ; it != set.End(); it++ )
      // 	std::cout << "    - " << (*it)->GetName() << std::endl;
      // std::cout << "-- end --" << std::endl;
    }

    const SetOf<UserItem>& 
    UserItem::GetDependentItems() const
    {
      return dependentItems_;
    }


    SetOf<UserItem>& 
    UserItem::GetDependentItems()
    {
      return dependentItems_;
    }

    SetOfBase<UserItem> 
    UserItem::GetAllDependentItems()
    {
      SetOfBase<UserItem> allDependentItems;
      ConcatenateDependentItems(allDependentItems);
      return allDependentItems;
    }


    bool
    UserItem::HasChild(const UserItem& item) const
    {
      UserItem& me = const_cast<UserItem&>(*this);
      const SetOfBase<UserItem> allChild = me.GetAllDependentItems();
      for (unsigned int i = 0; i < allChild.GetTotItemNumber(); ++i)
	{
	  const UserItem& current_item = allChild(i);
	  if (&current_item == &item)
	    return true;
	}
      return false;
    }

    void 
    UserItem::ConcatenateDependentItems(SetOfBase<UserItem>& allDependentItems)
    {
      SetOf<UserItem>& set = dependentItems_;
      SetOf<UserItem>::it it;
      for (it=set.Begin() ; it != set.End(); it++ )
	{
	  if (allDependentItems.Contain(**it) == false)
	    {
	      allDependentItems.AddItem(**it);
	      (*it)->ConcatenateDependentItems(allDependentItems);
	    }
	}
    }

    const std::string&
    UserItem::GetName() const
    {
      return name_;
    }

    std::string&
    UserItem::GetName()
    {
      return name_;
    }

    void
    UserItem::Update()
    {
      Item::Update();
      GetMainTreeItem().setText(0, GetName().c_str());
      UpdateIcon();
      GetMainTreeItem().Update();
      Core::SetOf<UserItem>::it it;
      for (it = dependentItems_.Begin() ; it != dependentItems_.End(); it++ )
	{
	  (*it)->Update();
	}
    }
    
    void
    UserItem::ChangeParentTreeItem(QTreeWidgetItem& newparent)
    {
      QTreeWidgetItem*  myparent = GetMainTreeItem().parent();
      
      if (myparent != &newparent)
	{
	  myparent->removeChild(mainTreeItem_);
	  newparent.addChild(mainTreeItem_);
	}
    }
    
    void
    UserItem::Select()
    {
      isSelected_ = true;

      SetOfBase<UserItem> set = GetAssociatedSelectedItem();
      SetOfBase<UserItem>::it it;
      for (it=set.Begin() ; it != set.End(); it++ )
	(*it)->Select();

    }

    void
    UserItem::UnSelect()
    {
      isSelected_ = false;
      SetOfBase<UserItem> set = GetAssociatedSelectedItem();
      SetOfBase<UserItem>::it it;
      for (it=set.Begin() ; it != set.End(); it++ )
	(*it)->UnSelect();
    }


    void
    UserItem::SetIsSelected(bool val)
    {
      isSelected_ = val;
    }


    bool
    UserItem::IsSelected() const
    {
      return isSelected_;
    }

    SetOfBase<UserItem> 
    UserItem::GetAssociatedSelectedItem()
    {
      return SetOfBase<UserItem>();
    }

    std::function<const Geom::Frame<_3D>& ()> 
    UserItem::GetFrameFct() const
    {
      const UserItem& me = *this;
      const Geom::Frame<_3D>&(UserItem::*fnpt)() const = &UserItem::GetFrame;
      std::function<const Geom::Frame<_3D>& ()> f = boost::bind(fnpt, boost::ref(me));
      return f;
    }


      const Geom::Frame<_3D>& 
      UserItem::GetReferenceFrame() const 
      {
	OMC_ASSERT_MSG(0, "This method can not be called from base class"); 
	return *(Geom::Frame<_3D>*)(0);
      }

      const Geom::Frame<_3D>& 
      UserItem::GetFrame() const
      {
	OMC_ASSERT_MSG(0, "This method can not be called from base class");
	return *(Geom::Frame<_3D>*)(0);
      }

      const Util::Color& 
      UserItem::GetColor() const 
      {
	OMC_ASSERT_MSG(0, "This method can not be called from base class");
	return *(Util::Color*)(0);
      }

      Util::Color& 
      UserItem::GetColor() 
      {
	OMC_ASSERT_MSG(0, "This method can not be called from base class");
	return *(Util::Color*)(0);
      }

    const OpenMeca::Item::Body& 
      UserItem::GetBody() const 
      {
	OMC_ASSERT_MSG(0, "This method can not be called from base class");
	return *(OpenMeca::Item::Body*)(0);
      }

      OpenMeca::Item::Body& 
      UserItem::GetBody()
      {
	OMC_ASSERT_MSG(0, "This method can not be called from base class");
	return *(OpenMeca::Item::Body*)(0);
      }

  
  }
}


