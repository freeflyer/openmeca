// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_Action_ActionEditSystemSetting_hpp
#define OpenMeca_Core_Action_ActionEditSystemSetting_hpp


namespace OpenMeca
{
  namespace Core
  {
    
    template <class T>
    class ActionEditSystemSetting
    {
    public:    
      static QString Text();
      static std::string Id();
      static QIcon Icon();
      static void DoAction(Action& action);
    };
    
    template<class T> 
    inline QString
    ActionEditSystemSetting<T>::Text()
    {
      return Action::tr("Edit");
    }

    template<class T> 
    inline std::string
    ActionEditSystemSetting<T>::Id()
    {
      return "Edit";
    }

    template<class T> 
    inline QIcon
    ActionEditSystemSetting<T>::Icon()
    {
      return SystemSettingT<T>::GetIcon();
    }

    template<class T>
    inline void
    ActionEditSystemSetting<T>::DoAction(Action& action)
    {
      T& t = SystemSettingT<T>::Get();
      Core::Singleton<Core::ItemCommonProperty<T> >::Get().GetDialog().Edit(action, t);
    }

  }
}


#endif
