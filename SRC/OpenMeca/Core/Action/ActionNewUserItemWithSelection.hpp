// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_Action_ActionNewUserItemWithSelection_hpp
#define OpenMeca_Core_Action_ActionNewUserItemWithSelection_hpp



#include "OpenMeca/Core/Action/ActionWithSelectedItemT.hpp"

namespace OpenMeca
{
  namespace Core
  {
    
    template <class T, class New>
    class ActionNewUserItemWithSelection
    {
      public:    
      static QString Text();
      static std::string Id();
      static QIcon Icon();
      template<class Action> static void DoAction(T&, Action& action);
    };
    
    template<class T, class New> 
    inline QString
    ActionNewUserItemWithSelection<T,New>::Text()
    {
      return Action::tr("New ") + New::GetQStrType(); 
    }

    template<class T, class New> 
    inline std::string
    ActionNewUserItemWithSelection<T,New>::Id()
    {
      return "New " + New::GetStrType(); 
    }

    template<class T, class New> 
    inline QIcon
    ActionNewUserItemWithSelection<T,New>::Icon()
    {
      QPixmap pixmap = Core::Singleton< Core::ItemCommonProperty<New> >::Get().GetIconSymbol().pixmap(14,14);
      QPainter painter(&pixmap);
      QPen pen(Qt::darkGreen);
      pen.setWidth(2);
      painter.setPen(pen);
      painter.drawLine (6, 9, 12,  9);
      painter.drawLine (9, 6,  9,  12);
      return QIcon(pixmap);
    }

    template<class T, class New>
    template<class Action> 
    inline void
    ActionNewUserItemWithSelection<T,New>::DoAction(T& item, Action& action)
    {
      Core::Singleton< Core::ItemCommonProperty<New> >::Get().GetDialog().New(action, item);
    }


  }
}


#endif
