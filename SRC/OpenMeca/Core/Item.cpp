// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.




#include "OpenMeca/Core/Item.hpp"
#include "OpenMeca/Core/System.hpp"

#include <boost/serialization/export.hpp>


namespace OpenMeca
{
  namespace Core
  {

    void
    Item::UnselectAll()
    {
      SetOf<Item>& set = AutoRegister<Item>::GetGlobalSet();
      SetOf<Item>::it it;
      for (it=set.Begin() ; it != set.End(); it++ )
	(*it)->SetIsSelected(false);
    }



    Item::Item(const std::string strType, QTreeWidgetItem* parent)
      :icon_(),
       mainTreeItem_(0), // Can't instanciate mainTreeItem here
       isSelected_(false),
       secondaryTreeItems_(),
       strType_(strType)
    {
      if (parent != 0)
	 mainTreeItem_ = new Gui::MainTreeItem(*parent, *this);
    }


    Item::~Item()
    {
      if (mainTreeItem_ != 0)
	delete mainTreeItem_;
    }


    System&
    Item::GetSystem()
    {
      return System::Get();
    }

    const std::string&
    Item::GetStrType() const
    {
      return strType_;
    }

    void
    Item::SaveState()
    {
    }

    void 
    Item::ResetState()
    {
    }

    void 
    Item::RecoveryState(unsigned int)
    {
    }

    void
    Item::MainTreeItemIsDeleted()
    {
      mainTreeItem_ = 0;
    }

    const std::string& 
    Item::GetName() const
    {
      return strType_;
    }

    void
    Item::Select()
    {
      isSelected_ = true;
    }

    void
    Item::UnSelect()
    {
      isSelected_ = false;
    }


    void
    Item::SetIsSelected(bool val)
    {
      isSelected_ = val;
    }


    bool
    Item::IsSelected() const
    {
      return isSelected_;
    }

    void 
    Item::AddSecondaryTreeItem(Gui::SecondaryTreeItem& item)
    {
      secondaryTreeItems_.AddItem(item);
    }

    void 
    Item::Update()
    {
      for (unsigned int i = 0; i < secondaryTreeItems_.GetTotItemNumber(); ++i)
	secondaryTreeItems_(i).Update();
    }

    void 
    Item::InitChSystem(chrono::ChSystem&)
    {
    }

    void 
    Item::BuildChSystem(chrono::ChSystem&)
    {
    }

    void 
    Item::UpdateValueFromCh()
    {
    }


  }
}


