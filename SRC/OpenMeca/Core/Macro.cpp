// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <boost/lexical_cast.hpp>

#include "OpenMeca/Core/Macro.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"

namespace OpenMeca
{
  namespace Core
  {

    void RaiseAssertion(int line, const std::string& file,
			const std::string& condition, const std::string& message)
    {

      if (Gui::MainWindow::Exist())
	{
	  QString text = "<b>" + QObject::tr("Sorry, openmeca encounters an error") + "</b><br><br>";
	  
	  text += QObject::tr("Error details :") + "<br>";
	  text += QObject::tr("source file :") + file.c_str() + "<br>";
	  text += QObject::tr("line :") + QString::number(line) + "<br>";
	  text += QObject::tr("condition :") + condition.c_str() + "<br>";
	  
	  if (!message.empty())
	    text += QObject::tr("message :") + message.c_str() + "<br>";
	  text += "<br><br>";
	  text += QObject::tr("The program may be unstable now, do you really want to continue ?") + "<br>";
	  Gui::MainWindow* win = &Gui::MainWindow::Get();
	  
	  int  stay = QMessageBox::critical(win, QObject::tr("Error"), text, 
					    QMessageBox::Yes|QMessageBox::No);
	  
	  if (stay == QMessageBox::Yes)
	    return;

	  QString saveStr = QObject::tr("Do You want to save your work before quitting OpenMeca ?");
	  int  save = QMessageBox::question(win, QObject::tr("Question"), saveStr, 
					    QMessageBox::Yes|QMessageBox::No);
	  if (save == QMessageBox::Yes)
	     win->SaveAs();

	}
      else
	{
          std::string text = "Sorry, openmeca encounters an error\n\n";
          text += "Error details :\n";
          text += "source file :" + file + "\n";
          text += "line :" + boost::lexical_cast<std::string>(line) + "\n";
	  text += "condition :" + condition + "\n";
	  
	  if (!message.empty())
	    text += "message :" + message + "\n";
	  text += "\n\n";
	  text += "The program will stop";
          std::cout << text << std::endl;

	}
      assert(0);
    }

    void DisplayInfoMsg(const QString& msg)
    {
      if (Gui::MainWindow::Exist())
	QMessageBox::information(&Gui::MainWindow::Get(), QObject::tr("Information"), msg);
      else
	std::cout << "[OpenMeca info:] " << msg.toStdString() << std::endl;
    }


    void DisplayWarningMsg(const QString& msg)
    {
      if (Gui::MainWindow::Exist())
	QMessageBox::warning(&Gui::MainWindow::Get(), QObject::tr("Warning"), msg);
      else
	std::cout << "[OpenMeca warn:] " << msg.toStdString() << std::endl;
    }


  }
}
