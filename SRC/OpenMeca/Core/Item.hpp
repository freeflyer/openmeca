// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_Item_hpp
#define OpenMeca_Core_Item_hpp


#include <QString>
#include <QIcon>

#include "OpenMeca/Gui/MainTreeItem.hpp"
#include "OpenMeca/Gui/SecondaryTreeItem.hpp"
#include "OpenMeca/Core/AutoRegister.hpp"
#include "OpenMeca/Core/Macro.hpp"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include "ChronoEngine/physics/ChSystem.h"

namespace OpenMeca
{
  namespace Core
  {

    class System;

    // Base class for all openmeca items : user or automatically created
    class Item : public AutoRegister<Item>
    {
    public:
      static void UnselectAll();

  
    public:    
      Item(const std::string strType_, QTreeWidgetItem* parent = 0);
      virtual ~Item();
      System& GetSystem();
      const std::string& GetStrType() const;

      // To save item's state during simulation
      virtual void SaveState();
      virtual void ResetState();
      virtual void RecoveryState(unsigned int);
      
      virtual const std::string& GetName() const;

      // Tree item
      void MainTreeItemIsDeleted();


      virtual void Select();
      virtual void UnSelect(); 
      virtual bool IsSelected() const;
      virtual void SetIsSelected(bool);
      virtual void Update();

      // Chrono::Engine management
      virtual void InitChSystem(chrono::ChSystem&);
      virtual void BuildChSystem(chrono::ChSystem&);
      virtual void UpdateValueFromCh();


      //Accessor
      OMC_ACCESSOR    (Icon        , QIcon            , icon_);
      OMC_ACCESSOR_PTR(MainTreeItem, Gui::MainTreeItem, mainTreeItem_);


    protected:
      void AddSecondaryTreeItem(Gui::SecondaryTreeItem&);


    protected :
      QIcon icon_;
      Gui::MainTreeItem* mainTreeItem_;
      bool isSelected_;
      SetOfBase<Gui::SecondaryTreeItem> secondaryTreeItems_;
      
    private :
      const std::string strType_;

    private :
      Item();                        //Not Allowed
      Item(const Item&);             //Not Allowed
      Item& operator=(const Item&);  //Not Allowed

      
      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive & ar, const unsigned int)
      {
	ar & BOOST_SERIALIZATION_NVP(const_cast<std::string&>(strType_));
      }
    }; 
    
  }
}




#endif
