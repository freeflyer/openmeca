// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Core/SystemSetting.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Util/Color.hpp"
#include "OpenMeca/Util/Icon.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"





namespace OpenMeca
{
  namespace Core
  {

    QTreeWidgetItem* SystemSetting::rootTreeWidgetItem_ = 0;
    
    std::map<const std::string, SystemSetting*> SystemSetting::instances_ = 
      std::map<const std::string, SystemSetting*>();

    void 
    SystemSetting::SaveAll(boost::archive::text_oarchive& ar, const unsigned int version)
    {
      const unsigned int instanceNumber = instances_.size();
      ar << instanceNumber;
      
      std::map<const std::string, SystemSetting*>::iterator it;
      for (it=instances_.begin(); it!=instances_.end(); ++it)
	{
	  const std::string key = it->first;
	  ar << key;
	  it->second->Save(ar, version);
	}
    }
    
    void 
    SystemSetting::LoadAll(boost::archive::text_iarchive& ar, const unsigned int version)
    {
      unsigned int instanceNumber = 0;
      ar >> instanceNumber;
      OMC_ASSERT_MSG(instanceNumber > 0,
		 "It's impossible that the number of instance equal zero");
      
      for (unsigned int i=0; i < instanceNumber; ++i)
	{
	  std::string key = "";
	  ar >> key;
	  OMC_ASSERT_MSG(key != "", "The key can't be null");
	  OMC_ASSERT_MSG(instances_.count(key) == 1, "The key " + key + " is not registered");
	  instances_[key]->Load(ar, version);
	}
    }
    
    const std::string
    SystemSetting::GetStrType()
    {
      return "System";
    }


    const QString
    SystemSetting::GetQStrType()
    {
      return QObject::tr("System");
    }

    QTreeWidgetItem& 
    SystemSetting::GetRootTreeWidgetItem()
    {
      if (rootTreeWidgetItem_ == 0)
	{
	  rootTreeWidgetItem_ = new QTreeWidgetItem(&(Gui::MainWindow::Get().GetTreeView()));
	  QIcon icon = Util::Icon::DrawIconFromSvgFile(":/Rsc/Img/" + QString(SystemSetting::GetStrType().c_str()) + ".svg");
	  rootTreeWidgetItem_->setIcon(0, icon);
	  rootTreeWidgetItem_->setText(0, GetQStrType());
	}
      return *rootTreeWidgetItem_;
    }
    
    SystemSetting::SystemSetting(const std::string strType)
      :Item(strType, &GetRootTreeWidgetItem()),
       strChildType_(strType)
    {
      OMC_ASSERT_MSG(instances_.count(strType) == 0,
		 "The instance is already registered");
      instances_[strType] = this;
    }

    
    SystemSetting::~SystemSetting()
    {
      instances_.erase(instances_.find(strChildType_)); 
    }
    
    void 
    SystemSetting::Update()
    {
    }



  }
}
 

