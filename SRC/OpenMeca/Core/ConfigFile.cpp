// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <iostream>

#include "OpenMeca/Core/ConfigFile.hpp"
#include "OpenMeca/Core/ConfigDirectory.hpp"
#include "OpenMeca/Core/Macro.hpp"



namespace OpenMeca
{
  namespace Core
  {

    std::map<const std::string, ConfigFile*> ConfigFile::all_ = std::map<const std::string, ConfigFile*>();

     void
     ConfigFile::RestoreAll()
     {
       OMC_ASSERT_MSG(all_.size() > 0, "Cannot find config file");
       std::map<const std::string, ConfigFile*>::iterator it;
       for (it=all_.begin(); it!=all_.end(); ++it)
	 it->second->RestoreBackup();
     }



    ConfigFile::ConfigFile(const std::string name, const std::string directory)
      :name_(name), 
       directory_(directory),
       file_(),
       backupFile_(":/Rsc/" + QString(directory_.c_str()) + "/" + QString(name_.c_str()))
    {
      OMC_ASSERT_MSG(backupFile_.exists(), 
                     "The backup file \"" +  name + "\" does not exist");
      
      OMC_ASSERT_MSG(all_.count(name_)==0, "This config file already exist");
      all_[name_]=this;
    }

    ConfigFile::~ConfigFile()
    {
      OMC_ASSERT_MSG(all_.count(name_)==1, "This config does not exist");
      all_.erase(name_);
    }
    
    QFile&
    ConfigFile::GetBackupFile()
    {
      return backupFile_;
    }

    QFile&
    ConfigFile::Open()
    {
      file_.close();
      QDir dir = GetConfigFileDirectory();
      
      const QString filePath = GetAbsPath();
      if (QFile::exists(filePath)==false)
	RestoreBackup();

      file_.setFileName(filePath);
      OMC_ASSERT_MSG(file_.open(QIODevice::ReadOnly | QIODevice::Text)==true,
		 "Can't open the required config file");
      return file_;
    }

    void 
    ConfigFile::RestoreBackup()
    {
      QDir dir = GetConfigFileDirectory();
      const QString filePath = GetAbsPath();
      if (QFile::exists(filePath)==true)
	{
	  file_.close();
	  file_.setFileName(filePath);
	  OMC_ASSERT_MSG(file_.remove(), 
			 "Can't remove the " + file_.fileName().toStdString() + " " +
			 "file in the config directory");
	}

      OMC_ASSERT_MSG(backupFile_.copy(filePath), 
		 "Can't copy the " + file_.fileName().toStdString() + " " +
		 "file in the config directory");
      QFile f(filePath);
      f.setPermissions(QFile::ReadOwner | QFile::WriteOwner | 
		       QFile::ReadUser  | QFile::WriteUser  |
		       QFile::ReadGroup | QFile::WriteGroup |
		       QFile::ReadOther | QFile::WriteOther);
      f.close();
    }

    QDir 
    ConfigFile::GetConfigFileDirectory()
    {
      QDir dir = ConfigDirectory::Get();
      
      if (dir.exists(directory_.c_str())==false)
	
OMC_ASSERT_MSG(dir.mkdir(directory_.c_str()),
		   "Can't make the config directory");
	  
      OMC_ASSERT_MSG(dir.cd(directory_.c_str())==true,
		 "Can't cd to the required directory");
      return dir;
    }

    QString
    ConfigFile::GetAbsPath()
    {
      QDir dir = GetConfigFileDirectory();
      const QString filePath = dir.absolutePath() + "/" + QString(name_.c_str());
      return filePath;
    }


  }
}




