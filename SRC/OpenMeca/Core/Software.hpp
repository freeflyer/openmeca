// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_Software_hpp
#define OpenMeca_Core_Software_hpp


#include <string>
#include <map>

#include "OpenMeca/Util/Version.hpp"

namespace OpenMeca
{
  namespace Core
  {

    class Software
    {
    public:
      static Software& Get();


    public:
      ~Software();
      void FakeTranslate();
      QString HtmlText() const;
      std::string GetInfoField(const std::string&) const;
      const Util::Version& Version() const;

    private :
      Software();                          
      Software(const Software&);             //Not Allowed
      Software& operator=(const Software&);  //Not Allowed
      
      void ReadInfo();
      

    private :
      static Software* me_;

    private:
      std::string fileInfo_;
      std::string rawContentInfofile_;
      std::map<const std::string, std::string> info_;
      Util::Version version_;
  
    }; 
    


  
  }
}
#endif
