// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_GlobalSettingInterfaceT_hpp
#define OpenMeca_Core_GlobalSettingInterfaceT_hpp

#include <QFile>
#include "OpenMeca/Core/GlobalSetting.hpp"
#include "OpenMeca/Core/Singleton.hpp"

namespace OpenMeca
{
  namespace Core
  {

    // Template class that allows static inheritence
    template<class T>
    class GlobalSettingT : public GlobalSetting, public Singleton<T>
    {
  
    public:    
      GlobalSettingT();
      virtual ~GlobalSettingT();
      
      
      
    private :
      GlobalSettingT(const GlobalSettingT&);             //Not Allowed
      GlobalSettingT& operator=(const GlobalSettingT&);  //Not Allowed
    }; 
    
    template<class T>
    inline
    GlobalSettingT<T>::GlobalSettingT()
      :GlobalSetting(T::GetStrType()),
       Singleton<T>()
    {
    }
    
    template<class T>
    inline
    GlobalSettingT<T>::~GlobalSettingT()
    {
    }

  }
}
#endif
