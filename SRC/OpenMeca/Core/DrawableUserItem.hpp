// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_DrawableUserItem_hpp
#define OpenMeca_Core_DrawableUserItem_hpp


#include <string>
#include <map>

#include "OpenMeca/Core/AutoRegister.hpp"
#include "OpenMeca/Core/UserItem.hpp"
#include "OpenMeca/Core/Drawable.hpp"

#include "OpenMeca/Geom/Point.hpp"
#include "OpenMeca/Geom/Quaternion.hpp"
#include "OpenMeca/Geom/Frame.hpp"

#include "OpenMeca/Util/Color.hpp"

namespace OpenMeca
{  
  namespace Item
  {  
    class Body;
  }
  
  namespace Core
  {

    // Base class for all 3D drawable items.
    class DrawableUserItem : public UserItem, public Drawable, public AutoRegister<DrawableUserItem>
    {
    
    public:
      static const std::string GetStrType(); 
      static DrawableUserItem& GetDrawableItemByGLKey(int);

    public:
      DrawableUserItem(const std::string strType, QTreeWidgetItem& parent, bool isSelectable);
      virtual ~DrawableUserItem();
            
      virtual void DrawShape()  = 0;
      virtual void BeginDraw(){};
      virtual void EndDraw(){};
      
      void Draw(bool withName = false);
      unsigned int GetItemGLKey() const;
      bool IsSelectable() const;


      //Accessor
      OMC_ACCESSOR(DrawLocalFrame, bool, drawLocalFrame_);


    private:
      DrawableUserItem(const DrawableUserItem&);             //Not Allowed
      DrawableUserItem& operator=(const DrawableUserItem&);  //Not Allowed

      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive& ar, const unsigned int);
      

    private:
      static int drawableItemCounter_;
      static std::map<int, DrawableUserItem*> map_;

    private:
      const unsigned int itemGLKey_;
      bool isSelectable_;
      bool drawLocalFrame_;

    };

    template<class Archive>
    void DrawableUserItem::serialize(Archive& ar, const unsigned int)
    {
       ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(UserItem);
       ar & BOOST_SERIALIZATION_NVP(drawLocalFrame_);
    }



  }

}




#endif
