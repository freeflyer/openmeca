// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Util_Dimension_hpp
#define OpenMeca_Util_Dimension_hpp

#include <string>
#include <map>

namespace OpenMeca
{
  namespace Util
  {
    class Unit;

    // A dimension (time, length) can be associated to many units
    class Dimension
    {
    public: 
      static Dimension& Get(const std::string);
      static bool Exist(const std::string);
      static std::string GetStrType();

      Dimension(std::string name);
      ~Dimension();
      void AddUnit(Unit&);
      void RemoveUnit(Unit&);

      void SetSiUnit(Unit&);
      void SetSiUnit();
      void SetUserChoiceUnit(Unit&);

      Unit& GetUnit(const std::string&);
      const Unit& GetUnit(const std::string&) const;

      const std::string& GetName() const;
      const std::map<std::string, Unit*>& GetUnits() const;
      const Unit& GetSiUnit() const;
      const Unit& GetUserChoice() const;
      
    private :
      static std::map<std::string, Dimension*> all_;

      const std::string name_;
      std::map<std::string, Unit*> units_;
      Unit* siUnit_;
      Unit* userChoice_;
    };
      
  }
}
#endif
