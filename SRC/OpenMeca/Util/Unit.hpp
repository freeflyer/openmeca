// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Util_Unit_hpp
#define OpenMeca_Util_Unit_hpp

#include <string>
#include <map>

namespace OpenMeca
{
  namespace Util
  {
    class Dimension;
      
    // A unit must be associated to a given dimension, for example 'second' unit
    // must be associated to time dimension
    class Unit
    {
    public: 
      static std::string GetStrType();

      Unit(Dimension& dimension, const std::string name, const std::string symbol, const double factor);
      ~Unit();
    
      const std::string& GetName() const;
      const std::string& GetSymbol() const;
      double GetFactor() const;
      const Dimension& GetDimension() const;

      
    private:
      Dimension& dimension_;
      const std::string name_; 
      const std::string symbol_;
      const double factor_;
    };
      
  }
}
#endif
