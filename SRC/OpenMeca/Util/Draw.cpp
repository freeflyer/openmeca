// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OSX
  #include <GL/glu.h>
#else
  #include <OpenGL/glu.h>
#endif

#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include "OpenMeca/Util/Draw.hpp"


namespace OpenMeca
{  
  namespace Util
  {
    struct XYZ
    {
      float x, y, z;
    };


     //Static methods that draw misceallous shape
    void
    Draw::Cylinder(float radius, float length, bool middlePositinning)
    {
      //Draw a cylinder along the x axis with given radius and length
      glRotatef(90., 0.0, 1.0, 0.0);
      static GLUquadric* quadric = gluNewQuadric();
      if (middlePositinning)
	glTranslatef(0.0f, 0.0f, -length/2);
      gluCylinder(quadric, radius, radius, length, 30, 1);
      glTranslatef(0.0f, 0.0f, length);
      gluDisk(quadric, 0.0, radius, 30, 1);
      glRotatef(180.0, 0.0, 1.0, 0.0);
      glTranslatef(0.0f, 0.0f, length);
      gluDisk(quadric, 0.0, radius, 30, 1);
      glTranslatef(0.0f, 0.0f, -length/2);
      glRotatef(-90.0, 0.0, 1.0, 0.0);
    }

    void
    Draw::CylinderStripe(float Re, float l)
    {
      glRotatef(90., 0.0, 1.0, 0.0);
      glBegin(GL_LINES);
      for (float i=0;i<=6.28;i=i+0.05)
	{
	  glVertex3f(Re*sin(i),Re*cos(i),-l/2.);
	  glVertex3f(Re*sin(i),Re*cos(i), l/2.);
	}
      glEnd();
      glRotatef(-90., 0.0, 1.0, 0.0);
    }

    void Draw::Cylinder(float externRadius, float internRadius, float length)
    {
      glRotatef(90., 0.0, 1.0, 0.0);
      const float Re = externRadius;
      const float Ri = internRadius;
      const float l  = length;
      static GLUquadric* quadric = gluNewQuadric();
      glTranslatef(0.0f, 0.0f, l/2);
      gluDisk(quadric, Ri, Re, 30, 1);
      glRotatef(180, 0.0, 1.0, 0.0);
      glTranslatef(0.0f, 0.0f, l);
      gluDisk(quadric, Ri, Re, 30, 1);
      
      double i;
      glBegin(GL_QUAD_STRIP);
      for (i=0;i<=6.28;i=i+0.2)
	{
	  glNormal3f(sin(i),cos(i),0.0f);
	  glVertex3f(Re*sin(i),Re*cos(i),0.0f);
	  glVertex3f(Re*sin(i),Re*cos(i),-l);
	}
      glNormal3f(0.0f,Re,0.0f);
      glVertex3f(0.0f,Re,0.0f);
      glVertex3f(0.0f,Re,-l);
      glEnd();
      
      glBegin(GL_QUAD_STRIP);
      for (i=0;i<=6.28;i=i+0.2)
	{
	  glNormal3f(-sin(i),-cos(i),0.0f);
	  glVertex3f(Ri*sin(i),Ri*cos(i),0.0f);
	  glVertex3f(Ri*sin(i),Ri*cos(i),-l);
	}
      glNormal3f(0.0f,-Ri,0.0f);
      glVertex3f(0.0f,Ri,0.0f);
      glVertex3f(0.0f,Ri,-l);
      glEnd();
    }


 void Draw::Cylinder(float externRadius, float internRadius, float length, float startAngle, float stopAngle)
    {
      glRotatef(90., 0.0, 1.0, 0.0);
      const float Re = externRadius;
      const float Ri = internRadius;
      const float l  = length;
      
      double i;
      glBegin(GL_QUAD_STRIP);
      for (i=startAngle;i<=stopAngle+0.1;i=i+0.1)
	{
	  glNormal3f(sin(i),cos(i),0.0f);
	  glVertex3f(Re*sin(i),Re*cos(i), -l/2.);
	  glVertex3f(Re*sin(i),Re*cos(i), l/2.);
	}
      
      glEnd();
      
      glBegin(GL_QUAD_STRIP);
      for (i=startAngle;i<=stopAngle+0.1;i=i+0.1)
	{
	  glNormal3f(-sin(i),-cos(i),0.0f);
	  glVertex3f(Ri*sin(i),Ri*cos(i), -l/2.);
	  glVertex3f(Ri*sin(i),Ri*cos(i),  l/2.);
	}
      glEnd();


      glBegin(GL_QUAD_STRIP);
      for (i=startAngle;i<=stopAngle+0.1;i=i+0.1)
	{
	  glNormal3f(0.0f, 0.0f, 1.f);
	  glVertex3f(Ri*sin(i),Ri*cos(i), -l/2.);
	  glVertex3f(Re*sin(i),Re*cos(i), -l/2.);
	  glNormal3f(0.0f, 0.0f, 1.f);
	}
      glEnd();

      glBegin(GL_QUAD_STRIP);
      for (i=startAngle;i<=stopAngle+0.1;i=i+0.1)
	{
	  glNormal3f(0.0f, 0.0f, -1.f);
	  glVertex3f(Ri*sin(i),Ri*cos(i), l/2.);
	  glVertex3f(Re*sin(i),Re*cos(i), l/2.);
	  glNormal3f(0.0f, 0.0f, -1.f);
	}
      glEnd();
    }

    void
    Draw::Sphere(float radius)
    {
      //Draw a cylinder along the x axis with given radius and length
      static GLUquadric* quadric = gluNewQuadric();
      gluSphere(quadric, radius, 30, 30);
    }

    void 
    Draw::Box(float lx, float ly, float lz)
    {
      const float Lx = lx/2;
      const float Ly = ly/2;
      const float Lz = lz/2;
      glBegin(GL_QUADS);
      glNormal3f(1.0f,0.0f,0.0f);
      glVertex3f(Lx,-Ly,-Lz);
      glVertex3f(Lx,Ly,-Lz);
      glVertex3f(Lx,Ly,Lz);
      glVertex3f(Lx,-Ly,Lz);
      glEnd();
      glBegin(GL_QUADS);
      glNormal3f(-1.0f,0.0f,0.0f);
      glVertex3f(-Lx,-Ly,-Lz);
      glVertex3f(-Lx,Ly,-Lz);
      glVertex3f(-Lx,Ly,Lz);
      glVertex3f(-Lx,-Ly,Lz);
      glEnd();
      glBegin(GL_QUADS);
      glNormal3f(0.0f,1.0f,0.0f);
      glVertex3f(Lx,Ly,Lz);
      glVertex3f(Lx,Ly,-Lz);
      glVertex3f(-Lx,Ly,-Lz);
      glVertex3f(-Lx,Ly,Lz);
      glEnd();
      glBegin(GL_QUADS);
      glNormal3f(0.0f,-1.0f,0.0f);
      glVertex3f(Lx,-Ly,Lz);
      glVertex3f(Lx,-Ly,-Lz);
      glVertex3f(-Lx,-Ly,-Lz);
      glVertex3f(-Lx,-Ly,Lz);
      glEnd();
      glBegin(GL_QUADS);
      glNormal3f(0.0f,0.0f,1.0f);
      glVertex3f(Lx,Ly,Lz);
      glVertex3f(Lx,-Ly,Lz);
      glVertex3f(-Lx,-Ly,Lz);
      glVertex3f(-Lx,Ly,Lz);
      glEnd();
      glBegin(GL_QUADS);
      glNormal3f(0.0f,0.0f,-1.0f);
      glVertex3f(Lx,Ly,-Lz);
      glVertex3f(Lx,-Ly,-Lz);
      glVertex3f(-Lx,-Ly,-Lz);
      glVertex3f(-Lx,Ly,-Lz);
      glEnd();
    }

    void 
    Draw::Channel(double Re,double Ri,double l)
    {
      static GLUquadric* quadric = gluNewQuadric();
      glTranslatef(-l/2, 0.0f, 0.0f );
      glBegin(GL_QUAD_STRIP);
      for (double i=0; i<=3.14; i=i+0.1)
	{
	  glNormal3f(0.0f,sin(i),cos(i));
	  glVertex3f(0.0f,Re*sin(i),Re*cos(i));
	  glVertex3f(l,Re*sin(i),Re*cos(i));
	}
      glNormal3f(0.0f,0.0f,-Re);
      glVertex3f(0.0f,0.0f,-Re);
      glVertex3f(l,0.0f,-Re);
      glEnd();
      
      glBegin(GL_QUAD_STRIP);
      for (double i=0; i<=3.14; i=i+0.1)
	{
	  glNormal3f(0.0f,-sin(i),-cos(i));
	  glVertex3f(0.0f,Ri*sin(i),Ri*cos(i));
	  glVertex3f(l,Ri*sin(i),Ri*cos(i));
	}

      glNormal3f(0.0f,0.0f,Ri);
      glVertex3f(0.0f,0.0f,-Ri);
      glVertex3f(l,0.0f,-Ri);
      glEnd();
      glTranslatef(l/2, 0.0f, 0.0f);
      
      //Plane that attach the semi-cylinder
      glBegin(GL_QUADS);
      glNormal3f(0.0f,-1.0f,0.0);
      glVertex3f(l/2,0.0f,Re);
      glVertex3f(l/2,0.0f,Ri);
      glVertex3f(-l/2,0.0f,Ri);
      glVertex3f(-l/2,0.0f,Re);
      glEnd();
      glBegin(GL_QUADS);
      glNormal3f(0.0f,-1.0f,0.0);
      glVertex3f(l/2,0.0f,-Re);
      glVertex3f(l/2,0.0f,-Ri);
      glVertex3f(-l/2,0.0f,-Ri);
      glVertex3f(-l/2,0.0f,-Re);
      glEnd();
      
      // Close the surface with partial disk
      glTranslatef(l/2, 0.0f, 0.0f);
      glRotatef(90.0, 0.0f, 1.0f, 0.0f);
      gluPartialDisk(quadric,Ri,Re,30,1,-90,180);
      glTranslatef(0.0f, 0.0f, -l);
      glRotatef(180.0, 0.0f, 1.0f, 0.0f);
      gluPartialDisk(quadric,Ri,Re,30,1,-90,180);
    }

    void 
    Draw::Cone(float r, float l)
    {
      static GLUquadric* quadric = gluNewQuadric();
      gluCylinder(quadric, 0.0f, r, l, 30, 1);
      glTranslatef(0.0f, 0.0f, l);
      gluDisk(quadric, 0.0, r, 30, 1);
    }

    void Draw::PartialSphere(double r,int n,int method,
			     double theta1,double theta2,double phi1,double phi2)
    {
      int i,j;
      double t1,t2,t3;
      XYZ e,p;
      
      for (j=0;j<n/2;j++) {
	t1 = phi1 + j * (phi2 - phi1) / (n/2);
	t2 = phi1 + (j + 1) * (phi2 - phi1) / (n/2);
	if (method == 0)
	  glBegin(GL_QUAD_STRIP);
	else
	  glBegin(GL_TRIANGLE_STRIP);
	for (i=0;i<=n;i++) {
	  t3 = theta1 + i * (theta2 - theta1) / n;
	  e.x = cos(t1) * cos(t3);
	  e.y = sin(t1);
	  e.z = cos(t1) * sin(t3);
	  p.x = r * e.x;
	  p.y = r * e.y;
	  p.z = r * e.z;
	  glNormal3f(e.x,e.y,e.z);
	  glTexCoord2f(i/(double)n,2*j/(double)n);
	  glVertex3f(p.x,p.y,p.z);
	  e.x = cos(t2) * cos(t3);
	  e.y = sin(t2);
	  e.z = cos(t2) * sin(t3);
	  p.x = r * e.x;
	  p.y = r * e.y;
	  p.z = r * e.z;
	  glNormal3f(e.x,e.y,e.z);
	  glTexCoord2f(i/(double)n,2*(j+1)/(double)n);
	  glVertex3f(p.x,p.y,p.z);
	}
	glEnd();
      }
    }

    void 
    Draw::OuterCylinderTeeth(float Ro, float mo, float alphao, float e)
    {
      const float Rb = Ro*cos(alphao);
      const float RTeethMax = Ro + mo;

      //Decalage pour que les dents soit aligné en degre
      float theta1 = (tan(alphao)-alphao)*180.0/M_PI;
      glRotatef(-theta1, 0.0f, 0.0f, 1.0f);

      //Decalage en degre pour le pas
      float theta2 = 2.0*(M_PI*mo/(4*Ro) + (tan(alphao)-alphao))*180.0/M_PI;
      float a = theta2*M_PI/(180.0);
      float Z = Ro*2.0/mo;
      float i,j,alt;

      for (j=0;j<Z;j=j+1)
	{
	  i=0;
	  glBegin(GL_QUAD_STRIP);
	  for (alt=0;alt<=RTeethMax;alt=Rb*(cos(i)+i*sin(i)))
	    {
	      glVertex3f(Rb*(cos(i)+i*sin(i)),Rb*(sin(i)-i*cos(i)), -e/2.0);
	      glVertex3f(Rb*(cos(i)+i*sin(i)),Rb*(sin(i)-i*cos(i)), e/2.0);
	      i=i+0.01;
	      glNormal3f(sin(i),-cos(i),0.0f);
	    }
	  //i = iMax;
	  glVertex3f(Rb*(cos(i)+i*sin(i)),Rb*(sin(i)-i*cos(i)), -e/2.0);
	  glVertex3f(Rb*(cos(i)+i*sin(i)),Rb*(sin(i)-i*cos(i)), e/2.0);
	  glNormal3f(sin(i),-cos(i),0.0f);
	  glEnd();
	  float recallPoint1X = Rb*(cos(i)+i*sin(i));
	  float recallPoint1Y = Rb*(sin(i)-i*cos(i));
	  glRotatef(theta2, 0.0, 0.0, 1.0);
	  glBegin(GL_QUAD_STRIP);
	  i=0.0;
	  for (alt=0;alt<=RTeethMax;alt=Rb*(cos(i)+i*sin(i)))
	    {
	      glVertex3f(Rb*(cos(i)+i*sin(i)),Rb*(sin(i)-i*cos(i)), -e/2.0);
	      glVertex3f(Rb*(cos(i)+i*sin(i)),Rb*(sin(i)-i*cos(i)), e/2.0);
	      i = i-0.01;
	      glNormal3f(-sin(i),cos(i),0.0f);
	    }
	  //i = iMax;
	  glVertex3f(Rb*(cos(i)+i*sin(i)),Rb*(sin(i)-i*cos(i)), -e/2.0);
	  glVertex3f(Rb*(cos(i)+i*sin(i)),Rb*(sin(i)-i*cos(i)), e/2.0);
	  glNormal3f(-sin(i),cos(i),0.0f);
	  glEnd();
	  glRotatef(-theta2, 0.0, 0.0, 1.0);
	  float recallPoint2X = Rb*(cos(i)+i*sin(i));
	  float recallPoint2Y = Rb*(sin(i)-i*cos(i));
	  float X2 = recallPoint2X*cos(a) - recallPoint2Y*sin(a);
	  float Y2 = recallPoint2X*sin(a) + recallPoint2Y*cos(a);
	  glBegin(GL_QUADS);
	  glVertex3f(recallPoint1X,recallPoint1Y, -e/2.0);
	  glVertex3f(recallPoint1X,recallPoint1Y, e/2.0);
	  glVertex3f(X2,Y2, e/2.0);
	  glVertex3f(X2,Y2, -e/2.0);
	  glNormal3f(cos(a/2.0),sin(a/2.0),0.0f);
	  glEnd();

	  glBegin(GL_QUAD_STRIP);
	  i=0.0;
	  for (alt=0;alt<=RTeethMax;alt=Rb*(cos(i)+i*sin(i)))
	    {
	      float recallPoint2X = Rb*(cos(-i)-i*sin(-i));
	      float recallPoint2Y = Rb*(sin(-i)+i*cos(-i));
	      float X2 = recallPoint2X*cos(a) - recallPoint2Y*sin(a);
	      float Y2 = recallPoint2X*sin(a) + recallPoint2Y*cos(a);
	      glVertex3f(Rb*(cos(i)+i*sin(i)),Rb*(sin(i)-i*cos(i)), e/2.0);
	      glVertex3f(X2,Y2, e/2.0);
	      i = i+0.01;
	      glNormal3f(0.0f,0.0f,1.0f);
	    }
	  glVertex3f(recallPoint1X,recallPoint1Y, e/2.0);
	  glVertex3f(X2,Y2, e/2.0);
	  glEnd();

	  glBegin(GL_QUAD_STRIP);
	  i=0.0;
	  for (alt=0;alt<=RTeethMax;alt=Rb*(cos(i)+i*sin(i)))
	    {
	      float recallPoint2X = Rb*(cos(-i)-i*sin(-i));
	      float recallPoint2Y = Rb*(sin(-i)+i*cos(-i));
	      float X2 = recallPoint2X*cos(a) - recallPoint2Y*sin(a);
	      float Y2 = recallPoint2X*sin(a) + recallPoint2Y*cos(a);
	      glVertex3f(Rb*(cos(i)+i*sin(i)),Rb*(sin(i)-i*cos(i)), -e/2.0);
	      glVertex3f(X2,Y2, -e/2.0);
	      i = i+0.01;
	      glNormal3f(0.0f,0.0f,-1.0f);
	    }
	  glVertex3f(recallPoint1X,recallPoint1Y, -e/2.0);
	  glVertex3f(X2,Y2, -e/2.0);
	  glEnd();

	  glRotatef(360.0/Z, 0.0, 0.0, 1.0);
	}
    }



    void Draw::InnerCylinderTeeth(float Ro, float mo,float alphao, float e)
    {
      const float Rb = Ro*cos(alphao);
      const float ITeethMin = mo;

      //Decalage pour que les dents soit aligné en degre
      float theta1 = (tan(alphao)-alphao)*180.0/M_PI;
      glRotatef(-theta1+180.0, 0.0, 0.0, 1.0);

      //Decalage en degre pour le pas
      float theta2 = 2.0*(M_PI*mo/(4*Ro) + (tan(alphao)-alphao))*180.0/M_PI;
      float Z = Ro*2.0/mo;
      float a = (theta2-360.0/Z)*M_PI/(180.0);

      const float rot = (360./Z)/2.;
      glRotatef(rot, 0.0f, 0.0f, 1.0f);
      

      float i,j,alt;

      for (j=0;j<Z;j=j+1)
	{
	  i=ITeethMin;
	  glBegin(GL_QUAD_STRIP);

	  for (alt=0;alt<=Ro+mo;alt=Rb*(cos(i)+i*sin(i)))
	    {
	      glVertex3f(Rb*(cos(i)+i*sin(i)),Rb*(sin(i)-i*cos(i)), -e/2.0);
	      glVertex3f(Rb*(cos(i)+i*sin(i)),Rb*(sin(i)-i*cos(i)), e/2.0);
	      i=i+0.01;
	      glNormal3f(-sin(i),cos(i),0.0f);
	    }

	  i=ITeethMin;
	  glBegin(GL_QUAD_STRIP);
	  for (alt=0;alt<=Ro+mo;alt=Rb*(cos(i)+i*sin(i)))
	    {

	      float recallPoint2X = Rb*(cos(-i)-i*sin(-i));
	      float recallPoint2Y = Rb*(sin(-i)+i*cos(-i));
	      float X2 = recallPoint2X*cos(a) - recallPoint2Y*sin(a);
	      float Y2 = recallPoint2X*sin(a) + recallPoint2Y*cos(a);
	      glVertex3f(X2,Y2, -e/2.0);
	      glVertex3f(X2,Y2, e/2.0);
	      i=i+0.01;
	      glNormal3f(sin(i),-cos(i),0.0f);
	    }

	  glBegin(GL_QUAD_STRIP);
	  i=ITeethMin;
	  for (alt=0;alt<=Ro+mo;alt=Rb*(cos(i)+i*sin(i)))
	    {
	      float recallPoint2X = Rb*(cos(-i)-i*sin(-i));
	      float recallPoint2Y = Rb*(sin(-i)+i*cos(-i));
	      float X2 = recallPoint2X*cos(a) - recallPoint2Y*sin(a);
	      float Y2 = recallPoint2X*sin(a) + recallPoint2Y*cos(a);
	      glVertex3f(Rb*(cos(i)+i*sin(i)),Rb*(sin(i)-i*cos(i)), e/2.0);
	      glVertex3f(X2,Y2, e/2.0);
	      i = i+0.01;
	      glNormal3f(0.0f,0.0f,1.0f);
	    }
	  glEnd();

	  glBegin(GL_QUAD_STRIP);
	  i=ITeethMin;
	  for (alt=0;alt<=Ro+mo;alt=Rb*(cos(i)+i*sin(i)))
	    {
	      float recallPoint2X = Rb*(cos(-i)-i*sin(-i));
	      float recallPoint2Y = Rb*(sin(-i)+i*cos(-i));
	      float X2 = recallPoint2X*cos(a) - recallPoint2Y*sin(a);
	      float Y2 = recallPoint2X*sin(a) + recallPoint2Y*cos(a);
	      glVertex3f(Rb*(cos(i)+i*sin(i)),Rb*(sin(i)-i*cos(i)), -e/2.0);
	      glVertex3f(X2,Y2, -e/2.0);
	      i = i+0.01;
	      glNormal3f(0.0f,0.0f,-1.0f);
	    }

	  glEnd();
	  glRotatef(360.0/Z, 0.0, 0.0, 1.0);
	}
    }

    void Draw::StraightTeeth(float mo, float alphao, float thickness, float length)
    {      
      const float step = M_PI*mo;
      const float hs = step/2.;
      const float c = 1.*mo*cos(alphao)/cos(alphao);
      const float s = 1.*mo*sin(alphao)/cos(alphao);
      const float e = thickness;

      const float start = int(int(length/2.)/step)*step - hs;

      glBegin(GL_QUAD_STRIP);
      for (float x = -start; x < length/2.; x += step)
	{

	  glVertex3f(x - s,  c, -e/2.0);
	  glVertex3f(x - s,  c,  e/2.0);
	  glNormal3f(-c, -s, .0f);

	  glVertex3f(x + s, -c, -e/2.0);
	  glVertex3f(x + s, -c,  e/2.0);
	  glNormal3f(-c, -s, .0f);

	  glVertex3f(x + hs - s, -c, -e/2.0);
	  glVertex3f(x + hs - s, -c,  e/2.0);
	  glNormal3f(.0f, -1.f, .0f);

	  glVertex3f(x + hs + s,  c, -e/2.0);
	  glVertex3f(x + hs + s,  c,  e/2.0);
	  glNormal3f(c, -s, .0f);

	  glVertex3f(x + 2.*hs - s,  c, -e/2.0);
	  glVertex3f(x + 2.*hs - s,  c,  e/2.0);
	  glNormal3f(.0f, -1.f, .0f);
	}
      glEnd();

      glBegin(GL_QUADS);

      for (float x = -start; x < length/2.; x += step)
      	{
      	  glNormal3f(0.f, 0.f, -1.f);
      	  glVertex3f(x - s,  c, -e/2.0);
      	  glVertex3f(x + s, -c, -e/2.0);
      	  glVertex3f(x + hs - s, -c, -e/2.0);
      	  glVertex3f(x + hs + s,  c, -e/2.0);
      	  glNormal3f(0.f, 0.f, -1.f);
      	}
      glEnd();

      glBegin(GL_QUADS);
      for (float x = -start; x < length/2.; x += step)
      	{
      	  glNormal3f(0.f, 0.f, 1.f);
      	  glVertex3f(x - s,  c, e/2.0);
      	  glVertex3f(x + s, -c, e/2.0);
      	  glVertex3f(x + hs - s, -c, e/2.0);
      	  glVertex3f(x + hs + s,  c, e/2.0);
      	  glNormal3f(0.f, 0.f, 1.f);
      	}
      glEnd();

      glTranslatef(.0f, c, .0f);
    }



    
  }
}




