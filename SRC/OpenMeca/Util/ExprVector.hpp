// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_ExprVector_hpp
#define OpenMeca_Item_ExprVector_hpp


#include <string>


#include "OpenMeca/Core/Macro.hpp"
#include "OpenMeca/Util/Expr.hpp"
#include "OpenMeca/Geom/Vector.hpp"


namespace OpenMeca
{  
  namespace Util
  {

    class ExprVector : public Geom::Vector<_3D>
    {

    public:
      ExprVector(std::function<const Geom::Frame<_3D>& ()> = &Geom::Frame<_3D>::GetGlobal);
      ExprVector(double, double, double, std::function<const Geom::Frame<_3D>& ()> = &Geom::Frame<_3D>::GetGlobal);
      virtual ~ExprVector();
      ExprVector(const ExprVector&) = default; 

      ExprVector& operator=(const Geom::Vector<_3D>&); 
      ExprVector& operator=(const ExprVector&); 

      void SetDimension(const Util::Dimension& dim);
      const Util::Dimension& GetDimension() const;

      OMC_ACCESSOR(ExpressionX, Util::Expr, expX_);
      OMC_ACCESSOR(ExpressionY, Util::Expr, expY_);
      OMC_ACCESSOR(ExpressionZ, Util::Expr, expZ_);

    private:      
      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive & ar, const unsigned int);

    private:
      Util::Expr expX_;
      Util::Expr expY_;
      Util::Expr expZ_;
    };


    template<class Archive>
    inline void
    ExprVector::serialize(Archive & ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Geom::Vector<_3D>);
      ar & BOOST_SERIALIZATION_NVP(expX_);
      ar & BOOST_SERIALIZATION_NVP(expY_);
      ar & BOOST_SERIALIZATION_NVP(expZ_);
    }
    

  }

}




#endif
