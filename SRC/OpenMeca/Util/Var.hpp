// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_Var_hpp
#define OpenMeca_Item_Var_hpp


#include <string>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include "OpenMeca/Util/exprtk.hpp"
#include "OpenMeca/Core/Macro.hpp"
#include "OpenMeca/Core/AutoRegister.hpp"

namespace OpenMeca
{  
  namespace Util
  {

    class Var : public Core::AutoRegister<Var>
    {

    public:
      static void UpdateTable();
      static exprtk::symbol_table<double>& GetSymbolTable();

      

    public:
      Var();
      Var(const std::string&, double&);

      ~Var();

      bool IsDoublon();
      bool IsValid();


      OMC_ACCESSOR(Symbol, std::string, symbol_);
      OMC_ACCESSOR(Value , double     , value_ );

    private:
      Var(const Var&);             //Not Allowed
      Var& operator=(const Var&);  //Not Allowed

      void AddToTable();

      
      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive & ar, const unsigned int);

    private:
      static exprtk::symbol_table<double> symbolTable_;

    private:
      std::string symbol_;
      double value_;
    };


    template<class Archive>
    inline void
    Var::serialize(Archive & ar, const unsigned int)
      {
	ar & BOOST_SERIALIZATION_NVP(symbol_);
	ar & BOOST_SERIALIZATION_NVP(value_);
      }


  }

}

#endif
