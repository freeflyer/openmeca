// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_ExprDouble_hpp
#define OpenMeca_Item_ExprDouble_hpp


#include <string>


#include "OpenMeca/Core/Macro.hpp"
#include "OpenMeca/Util/Expr.hpp"


namespace OpenMeca
{  
  namespace Util
  {

    class ExprDouble
    {

    public:
      ExprDouble();
      ExprDouble(double);
      virtual ~ExprDouble();
      ExprDouble(const ExprDouble&) = default; 
      
      ExprDouble& operator=(const double&); 
      ExprDouble& operator=(const ExprDouble&); 
      void Update();

      void SetDimension(const Util::Dimension& dim);
      const Util::Dimension& GetDimension() const;

      OMC_ACCESSOR(Expression, Util::Expr, exp_);
      OMC_ACCESSOR(Value     , double    , val_);

    private:      
      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive & ar, const unsigned int);

    private:
      double val_;
      Util::Expr exp_;
    };


    template<class Archive>
    inline void
    ExprDouble::serialize(Archive & ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_NVP(val_);
      ar & BOOST_SERIALIZATION_NVP(exp_);
    }
    

  }

}




#endif
