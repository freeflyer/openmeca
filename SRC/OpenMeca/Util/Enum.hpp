// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Util_Enum_hpp
#define OpenMeca_Util_Enum_hpp

#include <map>
#include <vector>
#include <string>
#include "OpenMeca/Core/Macro.hpp"


namespace OpenMeca
{
  namespace Util
  {
    
    // Util class to manage enum
    template<typename T> 
    class Enum
    {
    public:
      typedef std::map<std::string, T> Map;

      static std::string ToString(const T& val);      
      static T ToEnum(const std::string& str);
      static std::vector<std::string> List();

      static Map map_;
    };

    
        
    template<typename T> 
    std::string 
    Enum<T>::ToString(const T& val)
    {
      for (typename Map::iterator it=map_.begin(); it!=map_.end(); ++it)
	{
	  if (it->second == val)
	    return it->first;
	}
      OMC_ASSERT_MSG(0, "The val was not fond in the map");
      return "";
    }
    
    template<typename T> 
    T 
    Enum<T>::ToEnum(const std::string& str)
    {
      OMC_ASSERT_MSG(map_.count(str) == 1, "The key was not fond in the map");
      return map_[str];
    }

    template<typename T> 
    std::vector<std::string>
    Enum<T>::List()
    {
      std::vector<std::string> vec;
      for (typename Map::iterator it=map_.begin(); it!=map_.end(); ++it)
	vec.push_back(it->first);
      return vec;
    }


  }
}
#endif
